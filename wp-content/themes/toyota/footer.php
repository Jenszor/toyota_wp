<?php if($_COOKIE['cookienotice'] !== "true"){ ?>
<div class="row cookies" style="margin-top:0; background:#3a4146;">
<p><?php _e("Toyota Rent gebruikt cookies die je voorkeuren onthouden en het navigeren door de websites van Toyota Rent vergemakkelijken.", "Toyota Rent"); ?><a href="<?php echo get_page_link(304); ?>"> <?php _e("Meer weten", "Toyota Rent"); ?></a> <span class="cookieclose"><?php _e("Verder gaan", "Toyota Rent"); ?></span></p> 
</div>
<?php } ?> 
<div class="row footer">
    <div class="container">
        <?php

            $sitenav_args = array(
                'theme_location'  => 'site-nav',
                'menu'            => '',
                'container'       => '',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => 'footer-nav',
                'menu_id'         => '',
                'echo'            => true,
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul id="%1$s" class="%2$s"><h3>Sitemap</h3>%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
            );

            wp_nav_menu( $sitenav_args );

            languages_list_footer();

        ?>
        
    </div>
</div>

<?php wp_footer(); ?>
</body>
</html>
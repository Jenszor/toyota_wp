<?php
/*
Template Name: Reserveren
*/
?>
<?php get_header();
$user_ID = get_current_user_id(); 
$user_role = get_user_role($user_ID);
$shipping_class = get_user_meta($current_user->ID, "shipping_prices", true);
if(($shipping_class == "klasse1") or ($shipping_class == "")){
	$delivery = 85;
	$return = 170;
} 
if($shipping_class == "klasse2") {
	$delivery = 50;
	$return = 100;

}
if($shipping_class == "klasse3") {
	$delivery = "delivery";
	$return = "return";
}
?>
<div class="row main">
    <div class="container content">
  	   <h1><?php echo get_the_title(); ?></h1>
	   <div class="clearfix">

	     <div class="column_two_third gray_box" style="margin-top:10px;">
	        <div class="content-block">
	           	<?php if($_GET['message'] == "success"){ ?> 
		        	<h2 class="red" style="margin-top:20px;"><?php _e("Uw reservatie is verzonden", "Toyota Rent"); ?></h2>
		        	<p style="padding:20px 0;"><?php _e("Wij gaan na of de gekozen wagen beschikbaar is gedurende de geselecteerde periode. Wij bevestigen uw reservatie via e-mail.", "Toyota Rent"); ?></p>
					<div class="btn btn-purple-2">
						<a href="<?php echo get_permalink(2); ?>" style="color:#fff; display:block; text-transform:none;"><?php _e("Terug naar de homepagina", "Toyota Rent"); ?></a>
					</div>	
	        	<?php } else { ?>
	        	<form action="<?php bloginfo('template_url'); ?>/booking.php" method="post" name="reservation_form" id="reservation_form">

	        	<div id="errors">
					<p id="error_name"><?php _e("Gelieve uw voornaam in te vullen", "Toyota Rent"); ?></p>
					<p id="error_surname"><?php _e("Gelieve uw naam in te vullen", "Toyota Rent"); ?></p>
					<p id="error_street"><?php _e("Gelieve uw straat in te vullen", "Toyota Rent"); ?></p>
					<p id="error_nr"><?php _e("Gelieve uw huisnummer in te vullen", "Toyota Rent"); ?></p>
					<p id="error_postal"><?php _e("Gelieve uw postcode in te vullen", "Toyota Rent"); ?></p>
					<p id="error_city"><?php _e("Gelieve uw gemeente in te vullen", "Toyota Rent"); ?></p>
					<p id="error_birthdate"><?php _e("Gelieve uw geboortedatum in te vullen", "Toyota Rent"); ?></p>
					<p id="error_phone"><?php _e("Gelieve uw telefoonnumer in te vullen", "Toyota Rent"); ?></p>
					<p id="error_email"><?php _e("Gelieve uw e-mail adres in te vullen", "Toyota Rent"); ?></p>
					<p id="error_password"><?php _e("Gelieve een wachtwoord in te vullen", "Toyota Rent"); ?></p>

					<p id="error_vat"><?php _e("Het opgegeven BTW nummer is niet correct", "Toyota Rent"); ?></p>
					<p id="error_company"><?php _e("Gelieve uw bedrijfsnaam in te vullen", "Toyota Rent"); ?></p>
	        	</div>

	        	<?php
				if(is_user_logged_in()){
					$current_user = wp_get_current_user();
					$role = $current_user->roles;

					// var_dump($role); die();

					if((in_array("fleetowner", $role)) or (in_array("b2b", $role)) or (in_array("administrator", $role)) ){
					?>
					<div id="company" class="registration">   
					   <input type="hidden" name="enitity" id="entity" value="<?php echo $role[0]; ?>">  
					   <input type="hidden" name="shipping_class" id="shipping_class" value="<?php echo $shipping_class ?>">
					   <input type="hidden" name="user_id" id="user_id" value="<?php echo $current_user->ID; ?>">	
					   <input type="hidden" name="product_id" id="product_id" value="<?php echo $_COOKIE['product_id']; ?>">	 
					   <input type="hidden" name="total_cost" id="total_cost" value="<?php echo $_COOKIE['total_cost']; ?>">	 
				  	   <input type="hidden" name="reservation_from_date" id="reservation_from_date" value="<?php echo $_COOKIE['from_date']; ?>">	 
				   	   <input type="hidden" name="reservation_till_date" id="reservation_till_date" value="<?php echo $_COOKIE['end_date']; ?>">	           
		               <div class="block">
		                   <p><h2 class="red" style="margin-top:20px;"><?php _e("Bedrijfsgegevens", "Toyota Rent"); ?></h2></p>
			               <p><span><label for="company_name"><?php _e("Bedrijfsnaam", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_name" id="company_name" value="<?php echo get_user_meta($current_user->ID, "billing_company", true); ?>"/></span></p>
			               <p><span><label for="company_vat"><?php _e("BTW nummer", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_vat" id="company_vat" value="<?php echo get_user_meta($current_user->ID, "billing_vat", true); ?>"/></span></p>
		               </div>      
		               <div class="block">
			               <p><span><label for="company_street"><?php _e("Straat", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_street" id="company_street" value="<?php echo get_user_meta($current_user->ID, "billing_address_1", true); ?>"/></span></p>
			               <p><span><label for="company_nr"><?php _e("Huisnummer", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_nr" id="company_nr" value="<?php echo get_user_meta($current_user->ID, "billing_address_2", true); ?>"/></span></p>
			               <p><span><label for="company_postal"><?php _e("Postcode", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_postal" id="company_postal" value="<?php echo get_user_meta($current_user->ID, "billing_postcode", true); ?>"/></span></p>
			               <p><span><label for="company_city"><?php _e("Gemeente", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_city" id="company_city" value="<?php echo get_user_meta($current_user->ID, "billing_city", true); ?>"/></span></p>
			           </div>
		               <div class="block">
		               	   <p><h2 class="red" style="margin-top:20px;"><?php _e("Contactpersoon", "Toyota Rent"); ?></h2></p>
			               <p><span><label for="company_contact_name"><?php _e("Voornaam", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_contact_name" id="company_contact_name" value="<?php echo get_user_meta($current_user->ID, "billing_first_name", true); ?>"/></span></p>
			               <p><span><label for="company_contact_surname"><?php _e("Naam", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_contact_surname" id="company_contact_surname" value="<?php echo get_user_meta($current_user->ID, "billing_last_name", true); ?>"/></span></p>
			               <p><span><label for="company_contact_phone"><?php _e("Telefoon", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_contact_phone" id="company_contact_phone" value="<?php echo get_user_meta($current_user->ID, "billing_phone", true); ?>"/></span></p>
			               <p><span><label for="company_contact_email"><?php _e("E-mail", "Toyota Rent"); ?></label></span> <span><input type="email" name="company_contact_email" id="company_contact_email" value="<?php echo get_user_meta($current_user->ID, "billing_email", true); ?>"/></span></p>
		               </div>
		               <div class="block">		 
							<p><h2 class="red" style="margin-top:20px;"><?php _e("Levering / ophalen", "Toyota Rent"); ?></h2></p>
			               	<select name="shipping" id="shipping">
								<option value="0"><?php _e("Zelf afhalen bij Toyota Rent", "Toyota Rent"); ?> (&euro;0)</option>
								<option value="<?php echo $delivery; ?>"><?php _e("Wagen laten leveren", "Toyota Rent"); ?> (&euro;<?php echo $delivery * $vat; ?>)</option>
								<option value="<?php echo $return; ?>"><?php _e("Wagen laten leveren en ophalen", "Toyota Rent"); ?> (&euro;<?php echo $return * $vat; ?>)</option>
			               	</select>
		               </div> 	          
			        </div>
					<?php
					} else if(in_array("dealer", $role)){
					?>
					<div id="company" class="registration">   
					   <input type="hidden" name="entity" id="entity" value="<?php echo $role[0]; ?>">  
					   <input type="hidden" name="shipping_class" id="shipping_class" value="<?php echo get_user_meta($current_user->ID, "shipping_prices", true); ?>">
					   <input type="hidden" name="user_id" id="user_id" value="<?php echo $current_user->ID; ?>">	
					   <input type="hidden" name="product_id" id="product_id" value="<?php echo $_COOKIE['product_id']; ?>">	 
					   <input type="hidden" name="total_cost" id="total_cost" value="<?php echo $_COOKIE['total_cost']; ?>">	 
					   <input type="hidden" name="dealer_total_cost" id="dealer_total_cost" value="<?php echo $_COOKIE['dealer_total_cost']; ?>">	 
				  	   <input type="hidden" name="reservation_from_date" id="reservation_from_date" value="<?php echo $_COOKIE['from_date']; ?>">	 
				   	   <input type="hidden" name="reservation_till_date" id="reservation_till_date" value="<?php echo $_COOKIE['end_date']; ?>">
				   	   <div class="block">
		               	   <h2 class="red"><?php _e("Persoonlijke gegevens klant", "Toyota Rent"); ?></h2>
			               <p><span><label for="consumer_name"><?php _e("Voornaam", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_name" id="consumer_name"/></span></p>
			               <p><span><label for="consumer_surname"><?php _e("Naam", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_surname" id="consumer_surname"/></span></p>
		               </div>      
		               <div class="block">
			               <p><span><label for="consumer_street"><?php _e("Straat", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_street" id="consumer_street"/></span></p>
			               <p><span><label for="consumer_nr"><?php _e("Huisnummer", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_nr" id="consumer_nr"/></span></p>
			               <p><span><label for="consumer_postal"><?php _e("Postcode", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_postal" id="consumer_postal"/></span></p>
			               <p><span><label for="consumer_city"><?php _e("Gemeente", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_city" id="consumer_city"/></span></p>
			           </div>
		               <div class="block">
			               <p><span><label for="consumer_birthdate"><?php _e("Geboortedatum", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_birthdate" id="consumer_birthdate" class="datepicker"/></span></p>
			               <p><span><label for="consumer_phone"><?php _e("Telefoon", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_phone" id="consumer_phone"/></span></p>
			               <p><span><label for="consumer_email"><?php _e("E-mail", "Toyota Rent"); ?></label></span> <span><input type="email" name="consumer_email" id="consumer_email"/></span></p>
		               </div>
		               <div class="block">		 
							<p><h2 class="red" style="margin-top:20px;"><?php _e("Levering / ophalen", "Toyota Rent"); ?></h2></p>
			               	<select name="shipping" id="shipping" class="dealer_shipping">
								<option value="0"><?php _e("Zelf afhalen bij Toyota Rent", "Toyota Rent"); ?> (&euro;0)</option>
								<option value="85"><?php _e("Wagen laten leveren", "Toyota Rent"); ?> (&euro;102.85)</option>
								<option value="170"><?php _e("Wagen laten leveren en ophalen", "Toyota Rent"); ?> (&euro;217.80)</option>
			               	</select>
		               </div> 		           
		               <div class="block">
		                   <p><h2 class="red" style="margin-top:20px;"><?php _e("Bedrijfsgegevens", "Toyota Rent"); ?></h2></p>
			               <p><span><label for="company_name"><?php _e("Bedrijfsnaam", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_name" id="company_name" value="<?php echo get_user_meta($current_user->ID, "billing_company", true); ?>"/></span></p>
			               <p><span><label for="company_vat"><?php _e("BTW nummer", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_vat" id="company_vat" value="<?php echo get_user_meta($current_user->ID, "billing_vat", true); ?>"/></span></p>
		               </div>      
		               <div class="block">
			               <p><span><label for="company_street"><?php _e("Straat", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_street" id="company_street" value="<?php echo get_user_meta($current_user->ID, "billing_address_1", true); ?>"/></span></p>
			               <p><span><label for="company_nr"><?php _e("Huisnummer", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_nr" id="company_nr" value="<?php echo get_user_meta($current_user->ID, "billing_address_2", true); ?>"/></span></p>
			               <p><span><label for="company_postal"><?php _e("Postcode", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_postal" id="company_postal" value="<?php echo get_user_meta($current_user->ID, "billing_postcode", true); ?>"/></span></p>
			               <p><span><label for="company_city"><?php _e("Gemeente", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_city" id="company_city" value="<?php echo get_user_meta($current_user->ID, "billing_city", true); ?>"/></span></p>
			           </div>
		               <div class="block">
		               	   <p><h2 class="red" style="margin-top:20px;"><?php _e("Contactpersoon", "Toyota Rent"); ?></h2></p>
			               <p><span><label for="company_contact_name"><?php _e("Voornaam", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_contact_name" id="company_contact_name" value="<?php echo get_user_meta($current_user->ID, "billing_first_name", true); ?>"/></span></p>
			               <p><span><label for="company_contact_surname"><?php _e("Naam", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_contact_surname" id="company_contact_surname" value="<?php echo get_user_meta($current_user->ID, "billing_last_name", true); ?>"/></span></p>
			               <p><span><label for="company_contact_phone"><?php _e("Telefoon", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_contact_phone" id="company_contact_phone" value="<?php echo get_user_meta($current_user->ID, "billing_phone", true); ?>"/></span></p>
			               <p><span><label for="company_contact_email"><?php _e("E-mail", "Toyota Rent"); ?></label></span> <span><input type="email" name="company_contact_email" id="company_contact_email" value="<?php echo get_user_meta($current_user->ID, "billing_email", true); ?>"/></span></p>
		               </div>			            
			        </div>
			        <?php					
					} else {
					?>
		        	<div id="consumer" class="registration">
					   <input type="hidden" name="entity" id="entity" value="customer">  
					   <input type="hidden" name="shipping_class" id="shipping_class" value="<?php echo get_user_meta($current_user->ID, "shipping_prices", true); ?>">
					   <input type="hidden" name="user_id" id="user_id" value="<?php echo $current_user->ID; ?>">	
					   <input type="hidden" name="product_id" id="product_id" value="<?php echo $_COOKIE['product_id']; ?>">	 
					   <input type="hidden" name="total_cost" id="total_cost" value="<?php echo $_COOKIE['total_cost']; ?>">	 
				  	   <input type="hidden" name="reservation_from_date" id="reservation_from_date" value="<?php echo $_COOKIE['from_date']; ?>">	 
				   	   <input type="hidden" name="reservation_till_date" id="reservation_till_date" value="<?php echo $_COOKIE['end_date']; ?>">	           
		               <div class="block">
		               	   <h2 class="red"><?php _e("Persoonlijke gegevens", "Toyota Rent"); ?></h2>
			               <p><span><label for="consumer_name"><?php _e("Voornaam", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_name" id="consumer_name" value="<?php echo get_user_meta($current_user->ID, "first_name", true); ?>"/></span></p>
			               <p><span><label for="consumer_surname"><?php _e("Naam", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_surname" id="consumer_surname" value="<?php echo get_user_meta($current_user->ID, "last_name", true); ?>"/></span></p>
		               </div>      
		               <div class="block">
			               <p><span><label for="consumer_street"><?php _e("Straat", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_street" id="consumer_street" value="<?php echo get_user_meta($current_user->ID, "billing_address_1", true); ?>"/></span></p>
			               <p><span><label for="consumer_nr"><?php _e("Huisnummer", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_nr" id="consumer_nr" value="<?php echo get_user_meta($current_user->ID, "billing_address_2", true); ?>"/></span></p>
			               <p><span><label for="consumer_postal"><?php _e("Postcode", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_postal" id="consumer_postal" value="<?php echo get_user_meta($current_user->ID, "billing_postcode", true); ?>"/></span></p>
			               <p><span><label for="consumer_city"><?php _e("Gemeente", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_city" id="consumer_city" value="<?php echo get_user_meta($current_user->ID, "billing_city", true); ?>"/></span></p>
			           </div>
		               <div class="block">
			               <p><span><label for="consumer_birthdate"><?php _e("Geboortedatum", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_birthdate" id="consumer_birthdate" class="datepicker" value="<?php echo get_user_meta($current_user->ID, "birthdate", true); ?>"/></span></p>
			               <p><span><label for="consumer_phone"><?php _e("Telefoon", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_phone" id="consumer_phone" value="<?php echo get_user_meta($current_user->ID, "billing_phone", true); ?>"/></span></p>
			               <p><span><label for="consumer_email"><?php _e("E-mail", "Toyota Rent"); ?></label></span> <span><input type="email" name="consumer_email" id="consumer_email" value="<?php echo get_user_meta($current_user->ID, "billing_email", true); ?>"/></span></p>
		               </div>
		               <div class="block">		 
							<p><h2 class="red" style="margin-top:20px;"><?php _e("Levering / ophalen", "Toyota Rent"); ?></h2></p>
			               	<select name="shipping" id="shipping">
								<option value="pickup"><?php _e("Zelf afhalen bij Toyota Rent", "Toyota Rent"); ?> (&euro;0)</option>
								<option value="delivery"><?php _e("Wagen laten leveren", "Toyota Rent"); ?> (&euro;<?php echo $delivery * $vat; ?>)</option>
								<option value="return"><?php _e("Wagen laten leveren en ophalen", "Toyota Rent"); ?> (&euro;<?php echo $return * $vat; ?>)</option>
			               	</select>
		               </div> 	
		            </div>					
					<?php
					}
				} else {
	        	?>
	        	<div id="consumer" class="registration">
				   <input type="hidden" name="enitity" id="entity" value="customer">  	
				   <input type="hidden" name="product_id" id="product_id" value="<?php echo $_COOKIE['product_id']; ?>">	 
				   <input type="hidden" name="total_cost" id="total_cost" value="<?php echo $_COOKIE['total_cost']; ?>">	 
				   <input type="hidden" name="reservation_from_date" id="reservation_from_date" value="<?php echo $_COOKIE['from_date']; ?>">	 
				   <input type="hidden" name="reservation_till_date" id="reservation_till_date" value="<?php echo $_COOKIE['till_date']; ?>">	           
	               <div class="block">
	               	   <h2 class="red"><?php _e("Persoonlijke gegevens", "Toyota Rent"); ?></h2>
		               <p><span><label for="consumer_name"><?php _e("Voornaam", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_name" id="consumer_name"/></span></p>
		               <p><span><label for="consumer_surname"><?php _e("Naam", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_surname" id="consumer_surname"/></span></p>
	               </div>      
	               <div class="block">
		               <p><span><label for="consumer_street"><?php _e("Straat", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_street" id="consumer_street"/></span></p>
		               <p><span><label for="consumer_nr"><?php _e("Huisnummer", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_nr" id="consumer_nr"/></span></p>
		               <p><span><label for="consumer_postal"><?php _e("Postcode", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_postal" id="consumer_postal"/></span></p>
		               <p><span><label for="consumer_city"><?php _e("Gemeente", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_city" id="consumer_city"/></span></p>
		           </div>
	               <div class="block">
		               <p><span><label for="consumer_birthdate"><?php _e("Geboortedatum", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_birthdate" id="consumer_birthdate" class="datepicker"/></span></p>
		               <p><span><label for="consumer_phone"><?php _e("Telefoon", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_phone" id="consumer_phone"/></span></p>
		               <p><span><label for="consumer_email"><?php _e("E-mail", "Toyota Rent"); ?></label></span> <span><input type="email" name="consumer_email" id="consumer_email"/></span></p>
	               </div>
		               <div class="block">		 
							<p><h2 class="red" style="margin-top:20px;"><?php _e("Levering / ophalen", "Toyota Rent"); ?></h2></p>
			               	<select name="shipping" id="shipping">
								<option value="0"><?php _e("Zelf afhalen bij Toyota Rent", "Toyota Rent"); ?> (&euro;0)</option>
								<option value="<?php echo $delivery; ?>"><?php _e("Wagen laten leveren", "Toyota Rent"); ?> (&euro;<?php echo $delivery; ?>)</option>
								<option value="<?php echo $return; ?>"><?php _e("Wagen laten leveren en ophalen", "Toyota Rent"); ?> (&euro;<?php echo $return; ?>)</option>
			               	</select>
		               </div> 		
	            </div>
	            <?php } ?>		
		        <button type="submit" id="reservation_submit" class="btn-purple-2" value="<?php _e("Voltooien", "Toyota Rent"); ?>"><?php _e("Voltooien	", "Toyota Rent"); ?></button>
		        </form>
	        	<?php } ?>
	        </div>
	     </div>

	     <div class="column_one_third">

	         <div class="content-block blue_box">
	               <h2><?php _e("Uw reservatie", "Toyota Rent"); ?></h2>
	                <?php 
					$product = get_post($product_id); 
					$name = $product->post_name;

	                $product_id = $_COOKIE['product_id'];
	                $from_date = $_COOKIE['from_date'];
	                $till_date = $_COOKIE['end_date'];
	                $total_cost = $_COOKIE['total_cost'];

	                // Calculate VAT costs

	                $vat_cost = $total_cost * 0.21;
	                $full_price = $total_cost + $vat_cost;

					$thumbnail_id = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'post-thumbnail' );
   					$image = $thumbnail_id['0']; 

   					$url = get_permalink( $product_id );
   					echo '<a href="'.$url.'"><img src="'.$image.'" alt="car"/></a>'; 
   					echo '<h1 style="padding:10px 0;"><a style="color:#fff;" href="'.$url.'">'.get_the_title($product_id).'</a></h1>'; 
   					echo '<h3 style="padding:10px 0;"><span style="font-family:toyota_textregular;">'.__('Van', 'Toyota Rent').': </span> '.$from_date.' <span style="font-family:toyota_textregular;">'.__('Tot', 'Toyota Rent').': </span>'.$till_date.'</h3>'; 
   					echo '<input type="text" id="total_amount" value="€'. $total_cost .' '.__('Excl. BTW', 'Toyota Rent').';  €'. number_format((float)$full_price, 2, ',', '') .' '.__('Incl. BTW', 'Toyota Rent').'" disabled>';
   					?>
					<div class="btn btn-purple-2">
						<a href="<?php echo $url; ?>" style="color:#fff; display:block; text-transform:none;"><?php _e("Terug naar model", "Toyota Rent"); ?></a>
					</div>
	           </div>
	           <?php
				if(is_user_logged_in()){
				if($user_role == 'dealer'){ 
				$dealer_total_cost = $_COOKIE['dealer_total_cost'];
	  			$dealer_base_price = $_COOKIE['dealer_base_cost'];
	  			if(isset($price_discount)){ $dealer_base_price = $dealer_base_price * $price_discount; $dealer_base_price = number_format((float)$dealer_base_price, 2, '.', ''); }
				?>
				<div class="content-block blue_box">
		  			<h2 class="to-toggle"><?php _e("Dealer info", "Toyota Rent"); ?></h2>
					<div class="dealer-price">

						<p><?php _e("Prijs per dag:", "Toyota Rent"); echo " <span id='dealer_day_price'>&euro;".$dealer_base_price."</span>"; ?></p>
						<input type="text" id="dealer_total_amount" baseprice="<?php echo $dealer_total_cost; ?>" value="€<?php echo $dealer_total_cost; ?> excl. BTW" disabled>
						
					</div>
				</div>
				<?php
				}
				}
				?>

	     </div>

	  </div>
	</div>
</div>
<?php get_footer(); ?>
<?php
/*
Template Name: Lexus Models
*/
?>
<?php get_header(); 
$user_ID = get_current_user_id(); 
$user_role = get_user_role($user_ID);
if($user_role == "fleetowner"){
	$price_prefix = "fleet";
} elseif($user_role == "dealer"){
	$price_prefix = "dealer";
} elseif($user_role == "b2b"){
	$price_prefix = "b2b";
} else {
	$price_prefix = "part";
} 
if(isset($_SESSION['price_prefix'])){ unset($_SESSION['price_prefix']); } 
$_SESSION['price_prefix'] = $price_prefix; ?>
<div class="row main">
    <div class="container content">
  	   <h1><?php _e("Beschikbare modellen", "Toyota Rent"); ?> <span style="font-size:0.6em; margin-left:10px;"><?php if($vat == 1){ _e("excl. BTW", "Toyota Rent"); } else { _e("incl. BTW", "Toyota Rent"); } ?></span></h1>
	   <div class="clearfix">

	     <div class="column_two_third">
	           
	           <div class="models">
	           	<div class="clearfix">
	               <!--Content goes here-->
	               <?php
	                $models = get_terms("product_cat");
					 if ( !empty( $models ) && !is_wp_error( $models ) ){
					    foreach ( $models as $model ) {
	    				   $brand = get_field('merk', "{$model->taxonomy}_{$model->term_id}");
	    				   if($brand == "lexus"){
						       ?><div class="model-thumb" product-price-from="<?php do_action( 'price_from' ); ?>" product-price-to="<?php do_action( 'price_to' ); ?>" product-filters="<?php do_action( 'product_filters' ); ?>"><?php
						       echo '<div class="overlay"></div>';
						       //echo var_dump($model);
						       $title = $model->name;
						       $slug = $model->slug;
						       $thumbnail_id = get_woocommerce_term_meta( $model->term_id, 'thumbnail_id', true );
		    				   $image = wp_get_attachment_url( $thumbnail_id );
		    				   $url = get_permalink(92).$_SESSION['lang'];
		    				   echo '<a href="'.$url.'&model='.$slug.'"><img src="'.$image.'" alt="'.$title.'"/></a>';
		    				   echo '<a class="btn btn-purple-2" href="'.$url.'&model='.$slug.'">'.$title.'</a>';
		    				   $args = array(
							        'posts_per_page' => 1,
							        'post_type' => 'product',
							        'product_cat' => $slug,
                                    'meta_key' => 'partmaand',
                                    'meta_type' => 'NUMERIC',
                                    'orderby' => 'meta_value_num', 
							        'order' => 'ASC',
							   );
							   $model_query = new WP_Query( $args );
							   if ( $model_query->have_posts() ) :
									while ( $model_query->have_posts() ) : $model_query->the_post();
										//var_dump($model_query);
										$start_price = get_post_meta( $post->ID, $price_prefix."maand", true );
										$booking_pricing = get_post_meta( $post->ID, "_wc_booking_pricing", false );
										foreach ($booking_pricing as $key => $booking_pricing_value) {
											foreach ($booking_pricing_value as $key => $value) {
												if( ( $value["type"] == "custom" ) && ( strtotime($value["from"]) <= strtotime("now") ) && ( strtotime($value["to"]) >= strtotime("now") ) ){
													$sale_price = $start_price - $value["base_cost"];
													echo '<div class="sale_price"><a href="'.$url.'&model='.$slug.'">'.__('Vanaf', 'Toyota Rent').' €'.$start_price.'/dag</a></div>';
												}
											}
										}
										if(!empty($sale_price)){
											echo '<div class="start_price"><a href="'.$url.'&model='.$slug.'">'.__('Vanaf', 'Toyota Rent').' €'.$sale_price.'/dag</a></div>';
										} else {
											echo '<div class="start_price"><a href="'.$url.'&model='.$slug.'">'.__('Vanaf', 'Toyota Rent').' €'.$start_price.'/dag</a></div>';
										}
									endwhile;
								endif;
								wp_reset_postdata();
						       echo '</div>';
					       }
					    }
					 }
					 ?>
				</div>
	           </div>
				<div class="promotions">
					<?php woocommerce_get_template_part( 'content', 'promotions' ); ?>
				</div>
	     </div>

	     <div class="column_one_third">

	         <div class="content-block blue_box">
					<?php woocommerce_get_template_part( 'content', 'filters' ); ?>
	         </div>

	     </div>

	  </div>
	</div>
</div>
<?php get_footer(); ?>
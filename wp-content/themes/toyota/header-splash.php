<?php 
if($_GET['action'] == "login"){
	$user = $_POST['login_email'];
	$pass = $_POST['login_password'];

	$authenticate = login_custom_user($user, $pass);
	if($authenticate == "true"){
		header("location:".get_permalink(2));
	} else{
		header("location:".get_permalink(18)."&login=fail");
	}
}
if(isset($_GET['lang'])){
	$_SESSION['lang'] = "&lang=".$_GET['lang'];
} else {
	$_SESSION['lang'] = "";	
}
?><!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if lt IE 9]> <html class="old-ie"> <![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="row topbar">
    <div class="container">
        
        <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/toyota_logo.png" class="site-logo no-resp" alt="Toyota" /></a>

    </div>
</div>

<div class="header">
    
    <div class="navbar">
        <div class="container">


			<?php

			/*

			$sitenav_args = array(
				'theme_location'  => 'site-nav',
				'menu'            => '',
				'container'       => 'div',
				'container_class' => 'sitenav',
				'container_id'    => '',
				'menu_class'      => '',
				'menu_id'         => '',
				'echo'            => true,
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);

			wp_nav_menu( $sitenav_args );

			?>

			<?php
  */
			$sitenav_args = array(
				'theme_location'  => 'top-nav',
				'menu'            => '',
				'container'       => '',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'topnav',
				'menu_id'         => '',
				'echo'            => true,
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);
			

			if(is_user_logged_in()){
				 $current_user =	 wp_get_current_user();
			?>
				<ul id="menu-top-navigation" class="topnav">
					<li id="menu-item-25" class="welcome menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-18 current_page_item menu-item-25">
						<p href="http://localhost/toyota/?page_id=18"><?php _e("Hallo", "Toyota Rent"); ?> <?php echo $current_user->user_firstname; ?></p>
					</li>
					<li id="menu-item-24" class="register menu-item menu-item-type-post_type menu-item-object-page menu-item-24">
						<a href="<?php echo wp_logout_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"); ?>">Uitloggen</a>
					</li>
				</ul>
			<?php
			} else {
				wp_nav_menu( $sitenav_args );
			}
		?>

        </div>
    </div>
	
	

    <!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/header1.jpg" class="header-img" alt="Toyota Test Days" />
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/header1_mobile.jpg" class="header-img-mbl" alt="Toyota Test Days" />-->

</div>
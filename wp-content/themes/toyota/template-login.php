<?php
/*
Template Name: Login
*/
?>
<?php get_header(); ?>
<div class="row main">
    <div class="container content">
  	   <h1><?php echo get_the_title(); ?></h1>
	   <div class="clearfix">
	     <div class="column_two_third gray_box" style="margin-top:10px;">
	        <div class="content-block">
	        	<form action="<?php echo get_permalink( $post->ID ); ?>&action=login" method="post" name="login_form" id="login_form">
	        	<?php if($_GET['login'] == "fail"){ ?> <div class="message"><p><?php _e("U bent niet ingelogd. Controleer uw e-mail adres en wachtwoord", "Toyota Rent"); ?></p></div><?php } ?>	        	
	        	<div class="registration">
	               <div class="block">
		               <p><span><label for="consumer_name"><?php _e("E-mail adres", "Toyota Rent"); ?></label></span> <span><input type="text" name="login_email" id="login_email"/></span></p>
		               <p><span><label for="consumer_surname"><?php _e("Wachtwoord", "Toyota Rent"); ?></label></span> <span><input type="password" name="login_password" id="login_password"/></span></p>
	               </div>      
				</div>
		        <button type="submit" id="login_submit" class="btn-purple-2" value="<?php _e("Inloggen", "Toyota Rent"); ?>"><?php _e("Inloggen", "Toyota Rent"); ?></button>
		        </form>
	        </div>

	     </div>

	     <div class="column_one_third">

	         <div class="content-block blue_box">
	               <h2><?php _e("Openingsuren", "Toyota Rent"); ?></h2>
	               <table class="contact_table">
	                   <tr>
	                       <td><?php _e("Maandag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "maandag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Dinsdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "dinsdag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Woensdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "woensdag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Donderdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "donderdag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Vrijdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "vrijdag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Zaterdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "zaterdag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Zondag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "zondag", true); ?></td>
	                   </tr>
	               </table>
	           </div>

	     </div>

	  </div>
	</div>
</div>
<?php get_footer(); ?>
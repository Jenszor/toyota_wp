<?php
/*
Template Name: Contact Page
*/
?>
<?php get_header(); ?>
<div class="row main">
    <div class="container content">
  	   <h1><?php echo get_the_title(); ?></h1>
	   <div class="clearfix">

	     <div class="column_two_third gray_box" style="margin-top:10px;">
	           
	           <div class="content-block">
	               <h2 class="red"><?php echo get_post_meta( 12 , "naam", true); ?></h2>
	               <div class="block">
	              	 <address><?php echo get_post_meta( 12 , "adres", true); ?></address>
	               </div>

	               <div class="block">
		               <p><span><?php _e("Tel", "Toyota Rent"); ?></span> <span><?php echo get_post_meta( 12 , "tel", true); ?></span></p>
		               <p><span><?php _e("E-mail", "Toyota Rent"); ?></span> <span><?php echo get_post_meta( 12 , "email", true); ?></span></p>
	               </div>
					
					<div class="block">
		               <p><span><?php _e("BTW", "Toyota Rent"); ?></span> <span><?php echo get_post_meta( 12 , "btw", true); ?></span></p>
		               <p><span><?php _e("IBAN", "Toyota Rent"); ?></span> <span><?php echo get_post_meta( 12 , "iban", true); ?></span></p>
		               <p><span><?php _e("BIC", "Toyota Rent"); ?></span> <span><?php echo get_post_meta( 12 , "bic", true); ?></span></p>
	               </div>
	           </div>

	     </div>

	     <div class="column_one_third">

	         <div class="content-block blue_box">
	               <h2><?php _e("Openingsuren", "Toyota Rent"); ?></h2>
	               <table class="contact_table">
	                   <tr>
	                       <td><?php _e("Maandag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php the_field('maandag'); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Dinsdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php the_field('dinsdag'); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Woensdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php the_field('woensdag'); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Donderdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php the_field('donderdag'); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Vrijdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php the_field('vrijdag'); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Zaterdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php the_field('zaterdag'); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Zondag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php the_field('zondag'); ?></td>
	                   </tr>
	               </table>
	           </div>

	     </div>

	  </div>
	</div>
</div>
<?php get_footer(); ?>
<?php
$user_ID = get_current_user_id(); 
$user_role = get_user_role($user_ID);
if($user_role == "fleetowner"){
	$price_prefix = "fleet";
	$vat = 1;
} elseif($user_role == "dealer"){
	$price_prefix = "part";
	$vat = 1.21;
} elseif($user_role == "b2b"){
	$price_prefix = "b2b";
	$vat = 1;
} else {
	$price_prefix = "part";
	$vat = 1.21;
} 

if(isset($_SESSION['price_prefix'])){ unset($_SESSION['price_prefix']); } 
if(isset($_SESSION['vat'])){ unset($_SESSION['vat']); } 
$_SESSION['price_prefix'] = $price_prefix;
$_SESSION['vat'] = $vat;
?>
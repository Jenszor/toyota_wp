<?php
	if( is_user_logged_in() && is_front_page() ){
		wp_redirect(get_permalink(92));
	}
?>
<?php
	/* Template Name: Splash */
?>
<?php get_header( 'splash' ); ?>

<div id="splash">

	<div class="row main">

		<div class="container">


			<div id="choose">

				<h1><?php _e("Maak uw keuze om verder te gaan", "Toyota Rent"); ?></h1>
				
				<ul>

					<li><a href="<?php echo get_page_link(18).$_SESSION['lang']; ?>"><?php _e("Bedrijf", "Toyota Rent"); ?></a></li>
					<li><a href="<?php echo get_page_link(148).$_SESSION['lang']; ?>"><?php _e("Particulier", "Toyota Rent"); ?></a></li>

				</ul>

			</div>

		</div>

	</div>

</div>
<?php if(isset($_COOKIE['cookienotice'])){ ?>
<div class="row cookies">
<p><?php _e("Toyota Rent gebruikt cookies die je voorkeuren onthouden en het navigeren door de websites van Toyota Rent vergemakkelijken.", "Toyota Rent"); ?><a href="<?php echo get_page_link(304); ?>"> <?php _e("Meer weten", "Toyota Rent"); ?></a> <span class="cookieclose"><?php _e("Verder gaan", "Toyota Rent"); ?></span></p> 
</div>
<?php } get_footer(); ?>
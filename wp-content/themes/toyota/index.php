<?php get_header(); ?>

<div class="row main">
    <div class="container content">

	<?php if( have_posts() ) :
		while( have_posts() ) :
			the_post();
		?>

			<?php get_template_part('content', 'page' ); ?>

		<?php
		endwhile;
	else :
		?>
			<?php get_template_part('content', '404' ); ?>

		<?php endif;
	?>

    </div>
</div>

<?php get_footer(); ?>
<?php
/**
 * Get product price to filter field for model
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $model;
$slug = $model->slug;
$price_prefix = $_SESSION['price_prefix'];
$vat = $_SESSION['vat'];
if(empty($slug)){ $slug = $_SESSION['slug']; }
$args = array(
       'posts_per_page' => 1,
       'post_type' => 'product',
       'product_cat' => $slug,
       'orderby' => 'meta_value_num', 
       'meta_key' => $price_prefix.'maand',
       'order' => 'ASC', //same as from price for business logic reasons!
);
$price_query = new WP_Query( $args );
  if ( $price_query->have_posts() ) :
	while ( $price_query->have_posts() ) : $price_query->the_post();
		$postid = get_the_ID();
		$product_end_price = get_post_meta( $postid, $price_prefix."maand", true );
		$booking_pricing = get_post_meta( $postid, "_wc_booking_pricing", false );
		foreach ($booking_pricing as $key => $booking_pricing_value) {
			foreach ($booking_pricing_value as $key => $value) {
				if( ( $value["type"] == "custom" ) && ( strtotime($value["from"]) <= strtotime("now") ) && ( strtotime($value["to"]) >= strtotime("now") ) ){
					$product_end_price = $product_end_price * $value["cost"];
				}
			}
		}
	endwhile;
endif;
echo $product_end_price * $vat;
wp_reset_postdata();
?>
<?php global $wpo_wcpdf; ?>
<table class="head container">
	<tr>
		<td class="header">
		<?php
		if( $wpo_wcpdf->get_header_logo_id() ) {
			$wpo_wcpdf->header_logo();
		} else {
			echo apply_filters( 'wpo_wcpdf_invoice_title', __( 'Reservation confirmation', 'wpo_wcpdf' ) );
		}
		?>
		</td>
		<td class="shop-info">
			<div class="shop-name"><h3><?php $wpo_wcpdf->shop_name(); ?></h3></div>
			<div class="shop-address"><?php $wpo_wcpdf->shop_address(); ?></div>
		</td>
	</tr>
	<tr>
		<td>
			<h3 class="document-type-label">
			<?php echo _e( 'Reservation confirmation', 'wpo_wcpdf' ); ?>
			</h3>
			<?php do_action( 'wpo_wcpdf_after_document_label', 'invoice' ); ?>
		</td>
		<td>&nbsp;</td>
	</tr>

	<tr>
		<td>
			<div class="order-information">
			<?php if ( isset($wpo_wcpdf->settings->template_settings['display_number']) && $wpo_wcpdf->settings->template_settings['display_number'] == 'invoice_number') { ?>
				<span class="order-number-label"><?php _e( 'Invoice Number:', 'wpo_wcpdf' ); ?></span>
				<span class="order-number"><?php $wpo_wcpdf->invoice_number(); ?></span><br />
			<?php } ?>
			<?php if ( isset($wpo_wcpdf->settings->template_settings['display_date']) && $wpo_wcpdf->settings->template_settings['display_date'] == 'invoice_date') { ?>
				<span class="order-date-label"><?php _e( 'Invoice Date:', 'wpo_wcpdf' ); ?></span>
				<span class="order-date"><?php $wpo_wcpdf->invoice_date(); ?></span><br />
			<?php } ?>
				<span class="order-number-label"><?php _e( 'Order Number:', 'wpo_wcpdf' ); ?></span>
				<span class="order-number"><?php $wpo_wcpdf->order_number(); ?></span><br />
				<span class="order-date-label"><?php _e( 'Order Date:', 'wpo_wcpdf' ); ?></span>
				<span class="order-date"><?php $wpo_wcpdf->order_date(); ?></span><br />
			</div>
		</td>
		<td>
			<div class="recipient-address"><?php $wpo_wcpdf->shipping_address(); ?></div>
		</td>
	</tr>
</table><!-- head container -->

<?php do_action( 'wpo_wcpdf_before_order_details', 'invoice' ); ?>

<table class="order-details">
	<thead>
		<tr>
			<th class="product-label"><?php _e('Product', 'wpo_wcpdf'); ?></th>
			<th class="quantity-label"><?php _e('Quantity', 'wpo_wcpdf'); ?></th>
			<th class="price-label"><?php _e('Price', 'wpo_wcpdf'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php $items = $wpo_wcpdf->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item ) : ?>
		<tr>
			<td class="description">
				<?php $description_label = __( 'Description', 'wpo_wcpdf' ); // registering alternate label translation ?>
				<span class="item-name"><?php echo $item['name']; ?></span><span class="item-meta"><?php echo $item['meta']; ?></span> <span class="item-meta"><?php _e('From:', 'wpo_wcpdf'); echo " ".get_post_meta($wpo_wcpdf->export->order->id,'_order_date_from',true)." "; ?> <?php _e('Till:', 'wpo_wcpdf'); echo " ".get_post_meta($wpo_wcpdf->export->order->id,'_order_date_till',true); ?></span>
				<dl class="meta">
					<?php $description_label = __( 'SKU', 'wpo_wcpdf' ); // registering alternate label translation ?>
					<?php if( !empty( $item['sku'] ) ) : ?><dt><?php _e( 'SKU:', 'wpo_wcpdf' ); ?></dt><dd><?php echo $item['sku']; ?></dd><?php endif; ?>
					<?php if( !empty( $item['weight'] ) ) : ?><dt><?php _e( 'Weight:', 'wpo_wcpdf' ); ?></dt><dd><?php echo $item['weight']; ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
				</dl>
			</td>
			<td class="quantity"><?php echo $item['quantity']; ?></td>
			<td class="price"><?php echo "€".$item['item']['item_meta']['_line_total'][0]; ?></td>
		</tr>
		<?php endforeach; endif; ?>
		<tr>
			<td class="description"><?php _e( 'Delivery and return', 'wpo_wcpdf' ); ?></td>
			<td class="quantity">1</td>
			<td class="price"><?php echo "€".get_post_meta($wpo_wcpdf->export->order->id,'_order_shipping',true); ?></td>
		</tr>
	</tbody>
	<tfoot>
		<tr class="no-borders">
			<td class="no-borders" colspan="3">
				<table class="totals">
					<tfoot>
						<?php $x = 0; foreach( $wpo_wcpdf->get_woocommerce_totals() as $key => $total ) : ?>
						<?php if($x !== 0){ ?>
						<tr class="<?php echo $key; ?>">
							<td class="no-borders">&nbsp;</td>
							<th class="description"><?php echo $total['label']; ?></th>
							<td class="price"><span class="totals-price"><?php $total_value = $total['value']; echo $total_value; ?></span></td>
						</tr>
						<?php } ?>
						<?php $x++; endforeach; ?>
						<tr class="vat">
							<td class="no-borders">&nbsp;</td>
							<th class="description"><?php _e( 'VAT (21%)', 'wpo_wcpdf' ); ?></th>
							<td class="price"><span class="totals-vat"><?php $vat = substr($total_value, 27) * 0.21; echo "€".round($vat, 2); ?></span></td>
						</tr>
						<tr class="total">
							<td class="no-borders">&nbsp;</td>
							<th class="description"><?php _e( 'Order total', 'wpo_wcpdf' ); ?></th>
							<td class="price"><span class="totals-all"><?php $total = substr($total_value, 27); echo "€".round($vat + $total, 2); ?></span></td>
						</tr>
					</tfoot>
				</table>
			</td>

		</tr>
	</tfoot>
</table><!-- order-details -->

<?php do_action( 'wpo_wcpdf_after_order_details', 'invoice' ); ?>

<?php if ( $wpo_wcpdf->get_footer() ): ?>
<div id="footer">
	<?php $wpo_wcpdf->footer(); ?>
</div><!-- #letter-footer -->
<?php endif; ?>
<?php
/**
 * The template for displaying all promotions.
 *
 *
 */
 $args = array(
		'post_type' => 'promotions',
	);
	$promotions = new WP_Query( $args );
	if ( $promotions->have_posts() ) :
	?>
         <h1 style="margin-top:40px;"><?php _e("Promoties", "Toyota Rent"); ?></h1>
	<?php
		while ( $promotions->have_posts() ) : $promotions->the_post();
			$promotion_img = get_field('afbeelding');
			$promotion_url = get_field('url');
			echo '<div class="promotion"><a href="'.$promotion_url.'"><img src="'.$promotion_img.'" alt="promotie"/></a></div>';
		endwhile;
	endif;
	wp_reset_postdata();
?>
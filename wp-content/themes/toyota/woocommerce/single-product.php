<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
$tomorrow = new DateTime('tomorrow');
$tomorrow = $tomorrow->format('d/m/Y');
if(!isset($_COOKIE['from_date'])) {
    setcookie('from_date', date("d/m/Y"));
    $_COOKIE['from_date'] = date("d/m/Y");
}
if(!isset($_COOKIE['end_date'])) {
    setcookie('end_date', $tomorrow);
    $_COOKIE['end_date'] = $tomorrow;
}

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); 
global $price; 
?>
<div class="row main">
<?php while ( have_posts() ) : the_post(); 
	  $base_price = get_post_meta( $product->id, $price_prefix."maand", true );
	  $set_price = get_post_meta( $product->id, $price_prefix."maand", true );
	  $set_price = round($set_price * $vat, 2);
		$booking_pricing = get_post_meta( $product->id, "_wc_booking_pricing", false );
		foreach ($booking_pricing as $key => $booking_pricing_value) {
			foreach ($booking_pricing_value as $key => $value) {
				if( ( $value["type"] == "custom" ) && ( strtotime($value["from"]) <= strtotime("now") ) && ( strtotime($value["to"]) >= strtotime("now") ) ){
					$set_discount = $value["cost"];
					$set_price = $set_price * $value["cost"];
				}
			}
		}
	  if(isset($_SESSION['price'])){ unset($_SESSION['price']); } 
	  if(isset($_SESSION['base_price'])){ unset($_SESSION['base_price']); } 
	  $_SESSION['price'] = $set_price;
	  $_SESSION['base_price'] = $base_price;
	  ?>
	<div class="container content product_content" itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="product_title clearfix">
			<div class="product_name"><?php do_action( 'single_product_title' ); ?></div>
			<div class="product_price red_box"><?php do_action( 'single_product_price' ); ?></div>
		</div>
	 	<div class="clearfix">
	    	<div class="column_two_third">
	    		<div class="gray_box">
					<?php wc_get_template_part( 'content', 'single-product' ); ?>
				</div>				
				<div class="product_title clearfix" style="margin-top:40px;">
					<div class="product_name"><h1><?php _e("Upgrades", "Toyota Rent"); ?></h1></div>
					<div class="toggles">
						<span><img src="<?php bloginfo('template_url'); ?>/images/grid-btn.png" alt="grid" class="toggle_grid"/><img src="<?php bloginfo('template_url'); ?>/images/grid-btn-active.png" alt="grid" class="toggle_grid"/></span>
						<span><img src="<?php bloginfo('template_url'); ?>/images/list-btn.png" alt="list" class="toggle_list hide"/><img src="<?php bloginfo('template_url'); ?>/images/list-btn-active.png" alt="list" class="toggle_list"/></span>
					</div>
			  	</div>
				<?php
					$productid = array(get_the_ID());
					$categoryids = array();
					$categories = get_the_terms( $post->ID, 'product_cat' );
					foreach ($categories as $category) {
						    $product_cat_id = $category->slug;
						    array_push($categoryids, $product_cat_id);
						    break;
					}
					$catsid = $categoryids[0];
					$args = array(
						'post_type' => 'product',
						'taxonomy' => 'product_cat',
						'term' => $catsid,
						'post__not_in' => $productid,
						'meta_query' => array(
							array(
								'key'     => $price_prefix.'maand',
								'value'   => $base_price,
								'compare' => '>'
							)
						)
					);
					?>
				<!--GRID VIEW-->
				<div class="models specific grid hide">
					<?php
					$loop = new WP_Query( $args );
					if ( $loop->have_posts() ) {
						while ( $loop->have_posts() ) : $loop->the_post();
							//Create DIV
							echo '<div class="model-thumb">';
			       			echo '<div class="overlay"></div>';	
							$terms = get_the_terms( $post->ID, 'product_cat' );
							foreach ($terms as $term) {
							    $upgrade_cat = $term->name;
							    break;
							}			       			
							$title = str_replace($upgrade_cat." ", "", get_the_title());
						    $slug = $model->slug;
						    $thumbnail_id = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' );
		    				$image = $thumbnail_id['0'];
		    				$url = get_permalink( $post->ID );
		    				echo '<a href="'.$url.'"><img src="'.$image.'" alt="'.$title.'"/></a>';
		    				echo '<a class="btn btn-purple-2" href="'.$url.'">'.$title.'</a>';							//Display price
							$start_price = get_post_meta( $product->id, $price_prefix."maand", true );
							$start_price = round($start_price * $vat, 2);
							$booking_pricing = get_post_meta( $post->ID, "_wc_booking_pricing", false );
							foreach ($booking_pricing as $key => $booking_pricing_value) {
								foreach ($booking_pricing_value as $key => $value) {
									if( ( $value["type"] == "custom" ) && ( strtotime($value["from"]) <= strtotime("now") ) && ( strtotime($value["to"]) >= strtotime("now") ) ){
										$sale_price = $start_price * $value["cost"];
										echo '<div class="sale_price"><a href="'.$url.'">Vanaf €'.$start_price.'/dag</a></div>';
										echo '<div class="start_price"><a href="'.$url.'">Vanaf €'.$sale_price.'/dag</a></div>';							
									}
								}
							}
							if(empty($sale_price)){
								echo '<div class="start_price"><a href="'.$url.'">Vanaf €'.$start_price.'/dag</a></div>';
							}
							$sale_price = "";
							//Close DIV
			       			echo '</div>';
						endwhile;
					} else {
						echo __( 'Geen upgrades gevonden' );
					}
					wp_reset_postdata();
				?>
				</div>
				<!--END GRID VIEW-->
				<!--LIST VIEW-->
				<div class="list">
					<?php
					$loop = new WP_Query( $args );
					if ( $loop->have_posts() ) {
						while ( $loop->have_posts() ) : $loop->the_post();
							woocommerce_get_template_part( 'content', 'product_upgrades' );
						endwhile;
					} else {
						echo __( 'Geen upgrades gevonden' );
					}
					wp_reset_postdata();
				?>
				</div>
				<!--END LIST VIEW-->

			</div>

			<div class="column_one_third">
		         <div class="content-block blue_box">
		            <h2><?php _e("Kies uw periode", "Toyota Rent"); ?></h2>		
					<?php
						/**
						 * woocommerce_after_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
						 */
						//do_action( 'woocommerce_after_main_content' );
					?>
					<?php
					$min_duration = get_post_meta( $post->ID, "_wc_booking_min_duration", true );
					$max_duration = get_post_meta( $post->ID, "_wc_booking_max_duration", true );
					$booking_pricing = get_post_meta( $post->ID, "_wc_booking_pricing", false );
					foreach ($booking_pricing as $key => $booking_pricing_value) {
						foreach ($booking_pricing_value as $key => $value) {
							/*
							if( ($value["base_cost"] !== 0) && ($value["base_cost"] !== '') ){
							echo '<input type="hidden" class="pricing_table" range="'.$value["type"].'" operator="base" modifier="'.$value["base_modifier"].'" from="'.$value["from"].'" to="'.$value["to"].'" value="'.$value["base_cost"].'">';							
							}
							if( ($value["cost"] !== 0) or ($value["cost"] !== '') ){
							echo '<input type="hidden" class="pricing_table" range="'.$value["type"].'" operator="block" modifier="'.$value["modifier"].'" from="'.$value["from"].'" to="'.$value["to"].'" value="'.$value["cost"].'">';
							}
							*/
							if( ( $value["type"] == "custom" ) && ( strtotime($value["from"]) <= strtotime("now") ) && ( strtotime($value["to"]) >= strtotime("now") ) ){
								?> <input type="hidden" class="pricing_table" range="discount" operator="times" value="<?php echo $value["cost"]; ?>"> <?
							}
						}
					}
					?>
					<input type="hidden" class="pricing_table" range="blocks" operator="base" modifier="replace" from="1" to="4" value="<?php echo get_post_meta( $product->id, $price_prefix."1-4dagen", true ); ?>">
					<input type="hidden" class="pricing_table" range="blocks" operator="base" modifier="replace" from="5" to="30" value="<?php echo get_post_meta( $product->id, $price_prefix."5-30dagen", true ); ?>">
					<input type="hidden" class="pricing_table" range="blocks" operator="base" modifier="replace" from="31" to="365" value="<?php echo get_post_meta( $product->id, $price_prefix."maand", true ); ?>">
					<?php if($user_role == 'dealer'){ ?>
					<input type="hidden" class="pricing_table" range="dealer" operator="base" modifier="replace" from="1" to="4" value="<?php echo get_post_meta( $product->id, "dealer1-4dagen", true ); ?>">
					<input type="hidden" class="pricing_table" range="dealer" operator="base" modifier="replace" from="5" to="30" value="<?php echo get_post_meta( $product->id, "dealer5-30dagen", true ); ?>">
					<input type="hidden" class="pricing_table" range="dealer" operator="base" modifier="replace" from="31" to="365" value="<?php echo get_post_meta( $product->id, "dealermaand", true ); ?>">
					<? } ?>
					<input type="hidden" id="min_duration" value="<?php echo $min_duration; ?>">
					<input type="hidden" id="max_duration" value="<?php echo $max_duration; ?>">
					<input type="hidden" id="vat" value="<?php echo $vat; ?>">
					<input type="text" id="from_date" class="datepicker" value="<?php echo date("d/m/Y"); ?>">
					<input type="text" id="till_date" class="datepicker" value="<?php echo $tomorrow ?>">
		  		</div>		
		  		<div class="content-block blue_box">
		            <h2><?php _e("Totaalbedrag", "Toyota Rent"); if($user_role == "dealer"){ ?><br/><?php _e("Klantprijs BTW incl.", "Toyota Rent"); } ?></h2>		
					<?php
						/**
						 * woocommerce_after_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
						 */
						//do_action( 'woocommerce_after_main_content' );
					?>
					<input type="hidden" id="product_id" value="<?php echo $product->id; ?>">
					<input type="text" id="total_amount" baseprice="<?php echo $set_price; ?>" value="€<?php echo $set_price; ?>" disabled>
					
		  			<?php
		  				if( (is_user_logged_in()) && ($user_role !== "customer") ){
						$current_user =	 wp_get_current_user();
					?>
		  			<a class="btn btn-purple" href="<?php echo get_permalink(103).$_SESSION['lang']; ?>" id="place_order"><?php _e("Plaats boeking", "Toyota Rent"); ?></a>
		  			<?php } else { ?>
		  					<a class="btn btn-purple" href="http://nl.toyota.be/#/ajax/http%3A%2F%2Fnl.toyota.be%2Fforms%2Fforms.json%3Ftab%3Dpane-dealer%26WT.ac%3DCTA-sidebar-dealerfinder-NL-20141023" id="place_order"><?php _e("Zoek dealer en reserveer", "Toyota Rent"); ?></a>
		  			<?php }	?>
		  		</div>
		  		<?php
				if(is_user_logged_in()){
				if($user_role == 'dealer'){ 
	  			$dealer_base_price = get_post_meta( $product->id, "dealermaand", true );
	  			if(isset($price_discount)){ $dealer_base_price = $dealer_base_price * $price_discount; 
	  				$dealer_base_price = number_format((float)$dealer_base_price, 2, '.', ''); }
				?>
				<div class="content-block blue_box">
		  			<h2 class="to-toggle"><?php _e("Dealer info", "Toyota Rent"); ?></h2>
					<div class="dealer-price">

						<p><?php _e("Prijs per dag:", "Toyota Rent"); echo " <span id='dealer_day_price'>&euro;".$dealer_base_price."</span>"; ?></p>
						<input type="text" id="dealer_total_amount" baseprice="<?php echo $dealer_base_price; ?>" value="€<?php echo $dealer_base_price; ?> excl. BTW" disabled>
						
					</div>
				</div>
				<?php
				}
				}
				?>
		  	</div>
	  	</div>		
	</div>
<?php endwhile; // end of the loop. ?>
</div>	 

<?php get_footer( 'shop' ); ?>
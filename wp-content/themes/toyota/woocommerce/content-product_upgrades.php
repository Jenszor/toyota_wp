<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
$classes[] = 'gray_box upgrade';
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
?>
<div <?php post_class( $classes ); ?>>


	<a href="<?php the_permalink(); ?>">
		<div style="float:left;">
			<h2 class="red"><?php the_title(); ?></h2>
			<p><?php if(get_field('transmissie') == "manual"){ _e("Manueel", "Toyota Rent"); } else { _e("Automaat", "Toyota Rent"); } ?> / <?php if(is_array(get_field('airco'))){if(in_array('airco', get_field('airco'))){ _e("Airco", "Toyota Rent"); }} else { _e("Geen airco", "Toyota Rent"); } ?> / <?php the_field('aantal_zitplaatsen');echo " "; _e("zitplaatsen", "Toyota Rent"); ?> / <?php the_field('aantal_deuren'); echo " "; _e("deuren", "Toyota Rent"); ?> <?php if(is_array(get_field('gps'))){if(in_array('gps', get_field('gps'))){ echo ' / GPS'; }} ?></p>
		</div>
		<div class="product_price red_box" style="float:right;"><?php do_action( 'single_product_price_upgrade' ); ?></div>
	</a>


</div>
<?php
/**
 * Get product filter fields for model
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $model, $product;
$slug = $model->slug;
if(empty($slug)){ $slug = $product->slug; }
if(empty($slug)){ $slug = $_SESSION['slug']; }
$args = array(
     'posts_per_page' => -1,
     'post_type' => 'product',
     'product_cat' => $slug,
	 'orderby' => 'meta_value', 
	 'meta_key' => '_price',
     'order' => 'ASC',
);

$filter_fuel = array();
$filter_transmission = array();
$filter_seats = array();
$filter_doors = array();
$filter_airco = array();
$filter_gps = array();

$filter_query = new WP_Query( $args );
if ( $filter_query->have_posts() ) :
	while ( $filter_query->have_posts() ) : $filter_query->the_post();
		$postid = get_the_ID();
		$fuel = strtolower(get_post_meta( $postid, "brandstof", true ));
		$fuel = str_replace(" ", "-", $fuel);
		$transmission = get_post_meta( $postid, "transmissie", true );
		$seats = get_post_meta( $postid, "aantal_zitplaatsen", true );
		$doors = get_post_meta( $postid, "aantal_deuren", true );
		$airco = get_post_meta( $postid, "airco", true );
		if(empty($airco)){ $airco[0] = "no-ocria"; }
		$gps = get_post_meta( $postid, "gps", true );
		if(empty($gps)){ $gps[0] = "no-spg"; }
		array_push($filter_fuel, $fuel);
		array_push($filter_transmission, $transmission);
		array_push($filter_seats, $seats."-seats");
		array_push($filter_doors, $doors."-doors");
		array_push($filter_airco, $airco[0]);
		array_push($filter_gps, $gps[0]);
	endwhile;
endif;
wp_reset_postdata();

$filter_fuel = array_unique(array_filter($filter_fuel));
$filter_transmission = array_unique(array_filter($filter_transmission));
$filter_seats = array_unique(array_filter($filter_seats));
$filter_doors = array_unique(array_filter($filter_doors));
$filter_airco = array_unique(array_filter($filter_airco));
$filter_gps = array_unique(array_filter($filter_gps));

$filter_array = array_merge($filter_fuel, $filter_transmission, $filter_seats, $filter_doors, $filter_airco, $filter_gps);
foreach ($filter_array as $key => $value) {
	echo $value." ";
}

?>
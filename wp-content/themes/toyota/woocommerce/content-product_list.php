<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;
$price_prefix = $_SESSION['price_prefix'];
$vat = $_SESSION['vat'];

$postid = get_the_ID();
$fuel = strtolower(get_post_meta( $postid, "brandstof", true ));
$fuel = str_replace(" ", "-", $fuel);
$transmission = get_post_meta( $postid, "transmissie", true );
$seats = get_post_meta( $postid, "aantal_zitplaatsen", true );
$doors = get_post_meta( $postid, "aantal_deuren", true );
$airco = get_post_meta( $postid, "airco", true );
$product_from_price = get_post_meta( $post->ID, $price_prefix."maand", true );
$product_from_price = round($product_from_price * $vat, 2);
$booking_pricing = get_post_meta( $post->ID, "_wc_booking_pricing", false );									
foreach ($booking_pricing as $key => $booking_pricing_value) {
	foreach ($booking_pricing_value as $key => $value) {
		if( ( $value["type"] == "custom" ) && ( strtotime($value["from"]) <= strtotime("now") ) && ( strtotime($value["to"]) >= strtotime("now") ) ){
			$product_from_price = $product_from_price - $value["base_cost"];
		}
	}
}	
if(empty($airco)){ $airco[0] = "no-ocria"; }
$gps = get_post_meta( $postid, "gps", true );
if(empty($gps)){ $gps[0] = "no-spg"; }
// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
$classes[] = 'gray_box upgrade';
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
?>
<div <?php post_class( $classes ); ?> product-price-from="<?php echo $product_from_price; ?>" product-price-to="<?php echo $product_from_price; ?>" product-filters="<?php echo $fuel; ?> <?php echo $transmission; ?> <?php echo $seats; ?>-seats <?php echo $doors; ?>-doors <?php echo $airco[0]; ?> <?php echo $gps[0]; ?>">


	<a href="<?php the_permalink(); ?>">
		<div style="float:left;">
			<h2 class="red"><?php the_title(); ?></h2>
			<p><?php if(get_field('transmissie') == "manual"){ _e("Manueel", "Toyota Rent"); } else { _e("Automaat", "Toyota Rent"); } ?> / <?php if($airco[0] == "airco"){ _e("Airco", "Toyota Rent"); } else { _e("Geen airco", "Toyota Rent"); } ?> / <?php the_field('aantal_zitplaatsen');echo " "; _e("zitplaatsen", "Toyota Rent"); ?> / <?php the_field('aantal_deuren'); echo " "; _e("deuren", "Toyota Rent"); ?> <?php if(is_array(get_field('gps'))){if(in_array('gps', get_field('gps'))){ echo ' / GPS'; }} ?></p>
		</div>
		<div class="product_price red_box" style="float:right;"><?php do_action( 'single_product_price_list' ); ?></div>
	</a>


</div>
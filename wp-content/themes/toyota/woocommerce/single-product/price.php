<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
$user_ID = get_current_user_id(); 
$user_role = get_user_role($user_ID);
if($user_role == "fleetowner"){
	$price_prefix = "fleet";
} elseif($user_role == "dealer"){
	$price_prefix = "part";
} elseif($user_role == "b2b"){
	$price_prefix = "b2b";
} else {
	$price_prefix = "part";
}
$vat = $_SESSION['vat'];
$price = get_post_meta( $product->id, $price_prefix."maand", true );
$price = round($price * $vat, 2);
$booking_pricing = get_post_meta( $product->id, "_wc_booking_pricing", false );
foreach ($booking_pricing as $key => $booking_pricing_value) {
	foreach ($booking_pricing_value as $key => $value) {
		if( ( $value["type"] == "custom" ) && ( strtotime($value["from"]) <= strtotime("now") ) && ( strtotime($value["to"]) >= strtotime("now") ) ){
			$price = $price * $value["cost"];
		}
	}
}
?>
<!--<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

	<p class="price"><?php echo $product->get_price_html(); ?>/<?php _e("dag", "Toyota Rent"); ?></p>

	<meta itemprop="price" content="<?php echo $product->get_price(); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

</div>
-->
<p class="price"><?php _e("Vanaf", "Toyota Rent"); ?> €<?php echo $price; ?>/<?php _e("dag", "Toyota Rent"); ?> <span><?php if($vat == 1){ _e("excl. BTW", "Toyota Rent"); } else { _e("incl. BTW", "Toyota Rent"); } ?></span></p>

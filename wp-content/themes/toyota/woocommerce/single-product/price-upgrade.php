<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
$price_prefix = $_SESSION['price_prefix'];
$vat = $_SESSION['vat'];
$ref_price = $_SESSION['base_price'];
$price = get_post_meta( $product->id, $price_prefix."maand", true );
$markup_price = $price - $ref_price;
$markup_price = round($markup_price * $vat, 2);
?>
<span style="font-family:toyota_displayregular;"><?php _e("Slechts", "Toyota Rent"); ?> </span><?php echo '€'.$markup_price; ?>/<?php _e("dag extra", "Toyota Rent"); ?>
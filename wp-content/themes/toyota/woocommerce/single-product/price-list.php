<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
$price_prefix = $_SESSION['price_prefix'];
$vat = $_SESSION['vat'];
$ref_price = $_SESSION['base_price'];
$price = get_post_meta( $product->id, $price_prefix."maand", true );
$price = round($price * $vat, 2);
$booking_pricing = get_post_meta( $product->id, "_wc_booking_pricing", false );
foreach ($booking_pricing as $key => $booking_pricing_value) {
	foreach ($booking_pricing_value as $key => $value) {
		if( ( $value["type"] == "custom" ) && ( strtotime($value["from"]) <= strtotime("now") ) && ( strtotime($value["to"]) >= strtotime("now") ) ){
			$price = $price - $value["base_cost"];
		}
	}
}
?>
<span style="font-family:toyota_displayregular;"><?php _e("Vanaf", "Toyota Rent"); ?> </span><?php echo '€'.$price; ?>/<?php _e("dag", "Toyota Rent"); ?>
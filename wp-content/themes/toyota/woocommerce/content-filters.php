<?php
/**
 * The template for filters.
 *
 *
 */
?>
<h2><?php _e("Verfijn uw selectie", "Toyota Rent"); ?></h2>
<input type="text" id="start_date" class="datepicker" placeholder="Startdatum">
<input type="text" id="end_date" class="datepicker" placeholder="Einddatum">
<div class="price-slider">
	<div id="price-slider"></div>
	<p>
	  <?php _e("Tussen", "Toyota Rent"); ?> 
	  <span id="price-slider-from"></span>
	  <?php _e("en", "Toyota Rent"); ?> 
	  <span id="price-slider-to"></span>
	</p>
</div>
<select id="fuel" class="filter">
	<option value="" disabled selected><?php _e("Brandstof", "Toyota Rent"); ?></option>
	<option value="0"><?php _e("Alle", "Toyota Rent"); ?></option>
	<option value="benzine"><?php _e("Benzine", "Toyota Rent"); ?></option>
	<option value="diesel"><?php _e("Diesel", "Toyota Rent"); ?></option>
	<option value="hybride"><?php _e("Hybride", "Toyota Rent"); ?></option>
	<option value="plug-in-hybride"><?php _e("Plug-in Hybride", "Toyota Rent"); ?></option>						
</select>
<select id="transmission" class="filter">
	<option value="" disabled selected><?php _e("Transmissie", "Toyota Rent"); ?></option>
	<option value="0"><?php _e("Alle", "Toyota Rent"); ?></option>
	<option value="manual"><?php _e("Manuele transmissie", "Toyota Rent"); ?></option>
	<option value="auto"><?php _e("Automatische transmissie", "Toyota Rent"); ?></option>						
</select>
<select id="doors" class="filter">
	<option value="" disabled selected><?php _e("Aantal deuren", "Toyota Rent"); ?></option>
	<option value="0"><?php _e("Alle", "Toyota Rent"); ?></option>
	<option value="2">2 <?php _e("Deuren", "Toyota Rent"); ?></option>
	<option value="3">3 <?php _e("Deuren", "Toyota Rent"); ?></option>
	<option value="4">4 <?php _e("Deuren", "Toyota Rent"); ?></option>
	<option value="5">5 <?php _e("Deuren", "Toyota Rent"); ?></option>				
</select>
<select id="seats" class="filter">
	<option value="" disabled selected><?php _e("Zitplaatsen", "Toyota Rent"); ?></option>
	<option value="0"><?php _e("Alle", "Toyota Rent"); ?></option>
	<option value="2">2 <?php _e("Zitplaatsen", "Toyota Rent"); ?></option>
	<option value="3">3 <?php _e("Zitplaatsen", "Toyota Rent"); ?></option>
	<option value="4">4 <?php _e("Zitplaatsen", "Toyota Rent"); ?></option>
	<option value="5">5 <?php _e("Zitplaatsen", "Toyota Rent"); ?></option>		
	<option value="7">7 <?php _e("Zitplaatsen", "Toyota Rent"); ?></option>							
</select>
<select id="airco" class="filter">
	<option value="" disabled selected><?php _e("Airco", "Toyota Rent"); ?></option>
	<option value="0"><?php _e("Alle", "Toyota Rent"); ?></option>
	<option value="airco"><?php _e("Ja", "Toyota Rent"); ?></option>
	<option value="no-ocria"><?php _e("Neen", "Toyota Rent"); ?></option>						
</select>
<select id="gps" class="filter">
	<option value="" disabled selected><?php _e("GPS", "Toyota Rent"); ?></option>
	<option value="0"><?php _e("Alle", "Toyota Rent"); ?></option>
	<option value="gps"><?php _e("Ja", "Toyota Rent"); ?></option>
	<option value="no-spg"><?php _e("Neen", "Toyota Rent"); ?></option>						
</select>
<?php
/**
 * The template for displaying all models (product categories).
 *
 *
 */
$price_prefix = $_SESSION['price_prefix'];
$vat = $_SESSION['vat'];

$models = get_terms("product_cat");
if ( !empty( $models ) && !is_wp_error( $models ) ){
   foreach ( $models as $model ) {
			   $brand = get_field('merk', "{$model->taxonomy}_{$model->term_id}");
			   if($brand == "toyota"){
			   	$_SESSION['slug'] = $model->slug;
       ?><div class="model-thumb" product-price-from="<?php do_action( 'price_from' ); ?>" product-price-to="<?php do_action( 'price_to' ); ?>" product-filters="<?php do_action( 'product_filters' ); ?>"><?php
       echo '<div class="overlay"></div>';
       			$_SESSION['slug'] = "";
       //echo var_dump($model);
       	
       $title = $model->name;
       $slug = $model->slug;
       $thumbnail_id = get_woocommerce_term_meta( $model->term_id, 'thumbnail_id', true );
				   $image = wp_get_attachment_url( $thumbnail_id );
				   $url = get_permalink(92).$_SESSION['lang'];
				   echo '<a href="'.$url.'&model='.$slug.'"><img src="'.$image.'" alt="'.$title.'"/></a>';
				   echo '<a class="btn btn-purple-2" href="'.$url.'&model='.$slug.'">'.$title.'</a>';
				   $args = array(
	        'posts_per_page' => 1,
	        'post_type' => 'product',
	        'product_cat' => $slug,
                              'orderby' => 'meta_value_num', 
                              'meta_key' => $price_prefix.'maand',
	        'order' => 'ASC',
	   );
	   $model_query = new WP_Query( $args );
	   if ( $model_query->have_posts() ) :
			while ( $model_query->have_posts() ) : $model_query->the_post();
				//var_dump($model_query);
				$start_price = get_post_meta( $post->ID, $price_prefix."maand", true );
				$start_price = round($start_price * $vat, 2);				
				$booking_pricing = get_post_meta( $post->ID, "_wc_booking_pricing", false );
				foreach ($booking_pricing as $key => $booking_pricing_value) {
					foreach ($booking_pricing_value as $key => $value) {
						if( ( $value["type"] == "custom" ) && ( strtotime($value["from"]) <= strtotime("now") ) && ( strtotime($value["to"]) >= strtotime("now") ) ){
							$sale_price = $start_price - $value["base_cost"];
							echo '<div class="sale_price"><a href="'.$url.'&model='.$slug.'">'.__('Vanaf', 'Toyota Rent').' €'.$start_price.'/dag</a></div>';
						}
					}
				}
				if(!empty($sale_price)){
					echo '<div class="start_price"><a href="'.$url.'&model='.$slug.'">'.__('Vanaf', 'Toyota Rent').' €'.$sale_price.'/dag</a></div>';
				} else {
					echo '<div class="start_price"><a href="'.$url.'&model='.$slug.'">'.__('Vanaf', 'Toyota Rent').' €'.$start_price.'/dag</a></div>';
				}
			endwhile;
		endif;
		wp_reset_postdata();
       echo '</div>';
      }
   }
}
?>
<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
$vat = $_SESSION['vat'];
?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="car_overview">
		<h2 class="red"><?php _e("Over deze wagen", "Toyota Rent"); ?></h2>

		<?php 
						global $post;
						$terms = get_the_terms( $post->ID, 'product_cat' );
						foreach ($terms as $term) {
						    $product_cat_id = $term->term_id;
						    break;
						}
						$prod_term=get_term($product_cat_id,'product_cat');
						$product_cat_description=$prod_term->description;
						$product_slug=$prod_term->slug;
						echo '<p>'.$product_cat_description.'</p>';
						?>

		<h2 class="red"><?php _e("Foto's", "Toyota Rent"); ?></h2>

		<?php do_action( 'single_product_gallery' ); ?>
	</div>
	<div class="car_details">
		<div class="white_box">
			<p><img src="<?php bloginfo('template_url'); ?>/images/transmission.png" alt="<? _e("Transmissie", "Toyota Rent"); ?>"/> <?php if(get_field('transmissie') == "manual"){ _e("Manuele transmissie", "Toyota Rent"); } else { _e("Automatische transmissie", "Toyota Rent"); } ?></p>
			<p><img src="<?php bloginfo('template_url'); ?>/images/seats.png" alt="<? _e("Zitplaatsen", "Toyota Rent"); ?>"/><?php the_field('aantal_zitplaatsen');echo " "; _e("zitplaatsen", "Toyota Rent"); ?></p>
			<p><img src="<?php bloginfo('template_url'); ?>/images/doors.png" alt="<? _e("Deuren", "Toyota Rent"); ?>"/><?php the_field('aantal_deuren'); echo " "; _e("deuren", "Toyota Rent"); ?></p>
			<?php if(is_array(get_field('airco'))){if(in_array('airco', get_field('airco'))){ echo '<p><img src="'.get_bloginfo("template_url").'/images/airco.png" alt="Airco"/>'; _e("Airconditioning", "Toyota Rent"); echo "</p>"; }} ?>
			<?php if(is_array(get_field('gps'))){if(in_array('gps', get_field('gps'))){ echo '<p><img src="'.get_bloginfo("template_url").'/images/gps.png" alt="GPS"/>'; _e("GPS", "Toyota Rent"); echo "</p>"; }} ?>
			<?php if(is_array(get_field('trekhaak'))){if(in_array('trekhaak', get_field('trekhaak'))){ echo '<p><img src="'.get_bloginfo("template_url").'/images/trekhaak.png" alt="Trekhaak"/>'; _e("Trekhaak", "Toyota Rent"); echo "</p>"; }} ?>
			<?php if(get_field('winterbanden') !== "no"){ ?>
				<p><img src="<?php bloginfo('template_url'); ?>/images/winterbanden.png" alt="<? _e("Deuren", "Toyota Rent"); ?>"/><?php if(get_field('winterbanden') == "yes"){ _e("Met winterbanden", "Toyota Rent"); } else { _e("Winterbanden op aanvraag", "Toyota Rent"); } ?></p>
			<?php } ?>
			<p><img src="<?php bloginfo('template_url'); ?>/images/franchise.png" alt="<? _e("Franchise", "Toyota Rent"); ?>"/><?php _e("Vrijstelling ongeval: €", "Toyota Rent"); echo round(get_field('franchise') * $vat, 2); ?></p>
		</div>

		<div class="white_box back_button">
			<p style="text-transform:uppercase;"><a href="<?php echo get_permalink(92)."&model=".$product_slug; ?>"><?php _e("Terug naar overzicht", "Toyota Rent"); ?></a></p>
		</div>
	</div>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		//do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">

		<?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			//do_action( 'woocommerce_single_product_summary' );
		?>

	</div>

	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		//do_action( 'woocommerce_after_single_product_summary' );
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>

<?php
/*-----------------------------------------------------------------------------------*/
/* Remove Header Links */
/*-----------------------------------------------------------------------------------*/
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rel_canonical' ); // Display the canonical link rel
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );


/*-----------------------------------------------------------------------------------*/
/* Functions */
/*-----------------------------------------------------------------------------------*/
function login_custom_user ( $user='', $pass='') {

  if ( ! is_admin() ) {
      $creds = array();
      $creds['user_login'] = $user;
      $creds['user_password'] = $pass;
      $creds['remember'] = false;
      $user = wp_signon( $creds, false );
      if ( is_wp_error($user) == true ) {
        return false;
      } else {
        return true;
      }
  }
  
}
add_action( 'after_setup_theme', 'login_custom_user', 0, 2 );

function get_user_role($uid) {
  global $wpdb;
  $role = $wpdb->get_var("SELECT meta_value FROM {$wpdb->usermeta} WHERE meta_key = 'wp_capabilities' AND user_id = {$uid}");
    if(!$role) return 'non-user';
  $rarr = unserialize($role);
  $roles = is_array($rarr) ? array_keys($rarr) : array('non-user');
  return $roles[0];
}
add_action( 'after_setup_theme', 'get_user_role', 0, 2 );

/*-----------------------------------------------------------------------------------*/
/* Theme Settings */
/*-----------------------------------------------------------------------------------*/
// Disable admin-bar
//add_filter('show_admin_bar', '__return_false');

// Post Thumbnail Init
add_theme_support( 'post-thumbnails' );
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

/*-----------------------------------------------------------------------------------*/
/* Localisation */
/*-----------------------------------------------------------------------------------*/
add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup(){
    load_theme_textdomain('Toyota Rent', get_template_directory() . '/languages');
}

/*-----------------------------------------------------------------------------------*/
/* Project Enqueues */
/*-----------------------------------------------------------------------------------*/

// JAVASCRIPT
add_action( 'wp_enqueue_scripts', 'custom_enqueue_scripts' ); 
function custom_enqueue_scripts() {

  // jQuery
  wp_deregister_script('jquery');
  wp_register_script( 'jquery', 'http://code.jquery.com/jquery-latest.min.js');
  wp_enqueue_script( 'jquery' );

  // Custom JS
  wp_enqueue_script( 'validation-js', get_template_directory_uri() . '/js/validation/dist/jquery.validate.min.js');
  wp_enqueue_script( 'cookie-js', get_template_directory_uri() . '/js/jquery.cookie.js');
  wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/js/custom.min.js');
  wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/js/jquery-ui.js' );
}

add_action( 'admin_enqueue_scripts', 'custom_admin_enqueue_scripts' ); 
function custom_admin_enqueue_scripts() {
  wp_enqueue_script( 'admin-js', get_template_directory_uri() . '/js/custom-admin.min.js');
}

// CSS
add_action( 'wp_enqueue_scripts', 'custom_enqueue_styles' ); 
function custom_enqueue_styles() {

  // Google Fonts
  // wp_register_style('google-bitter', 'http://fonts.googleapis.com/css?family=Bitter:400,700');
  // wp_enqueue_style( 'google-bitter');

  // Custom CSS
  wp_enqueue_style( 'jquery-ui-css', '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css', '', '1.1', all );
  wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', '', '1.1', all );
  wp_enqueue_style( 'splash', get_template_directory_uri() . '/css/splash.css', '', '1.1', all );

}


/*-----------------------------------------------------------------------------------*/
/* Menus init */
/*-----------------------------------------------------------------------------------*/
add_action( 'init', 'register_menus' );

function register_menus() {
  register_nav_menus(
    array(
      'site-nav' => __( 'Site Navigation', 'custom_theme' ),
      'top-nav'  => __( 'Top Navigation', 'custom_theme' )
    )
  );
}

function languages_list_footer(){
    $languages = icl_get_languages('skip_missing=0&order=desc&orderby=code');
    if(!empty($languages)){
        echo '<ul class="footer-nav"><h3>Taal</h3>';
        foreach($languages as $l){
            echo '<li>';
            if(!$l['active']) echo '<a href="'.$l['url'].'">';
            echo icl_disp_language($l['native_name'], $l['translated_name']);
            if(!$l['active']) echo '</a>';
            echo '</li>';
        }
        echo '</ul>';
    }
}


/*-----------------------------------------------------------------------------------*/
/* WooCommerce */
/*-----------------------------------------------------------------------------------*/

add_action('single_product_title', 'get_single_product_title');

function get_single_product_title() {
    wc_get_template( 'single-product/title.php' );
}

add_action('single_product_price', 'get_single_product_price');

function get_single_product_price() {
    wc_get_template( 'single-product/price.php' );
}

add_action('single_product_price_upgrade', 'get_single_product_price_upgrade');

function get_single_product_price_upgrade() {
    wc_get_template( 'single-product/price-upgrade.php' );
}

add_action('single_product_price_list', 'get_single_product_price_list');

function get_single_product_price_list() {
    wc_get_template( 'single-product/price-list.php' );
}

add_action('single_product_gallery', 'get_single_product_gallery');

function get_single_product_gallery() {
    wc_get_template( 'single-product/product-thumbnails.php' );
}

add_action('product_filters', 'get_product_filters');

function get_product_filters() {
    wc_get_template( 'product-filters.php' );
}

add_action('price_from', 'get_price_from');

function get_price_from() {
    wc_get_template( 'product-price_from.php' );
}

add_action('price_to', 'get_price_to');

function get_price_to() {
    wc_get_template( 'product-price_to.php' );
}

//  remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );

/*-----------------------------------------------------------------------------------*/
/* Image Sizes */
/*-----------------------------------------------------------------------------------*/
//add_image_size( 'header', 1600, true );
//update_option('thumbnail_size_w', 280);
//update_option('thumbnail_size_h', 168);
//update_option('medium_size_w', 500);
//update_option('medium_size_h', 300);
//update_option('large_size_w', 680);
//update_option('large_size_h', 410);


/*-----------------------------------------------------------------------------------*/
/* Selectively Hide WP Admin Menus */
/*-----------------------------------------------------------------------------------*/
// add_action('admin_init', 'ns_remove_admin_menus');
// function ns_remove_admin_menus() {

//   global $current_user;

//   if ($current_user->ID != 2) {

//     // Remove menu pages
//     //remove_menu_page('edit.php');
//     remove_menu_page('plugins.php');
//     remove_menu_page('options-general.php');
//     remove_menu_page('tools.php');
//     remove_menu_page('edit.php?post_type=acf');
//     remove_submenu_page( 'themes.php', 'themes.php' );
//     remove_submenu_page( 'themes.php', 'customize.php' );
//     remove_submenu_page( 'themes.php', 'theme-editor.php' );

//     // Remove WP Update nag
//     add_action('admin_menu','wphidenag');
//     function wphidenag() {
//       remove_action( 'admin_notices', 'update_nag', 3 );
//     }

//     // Remove Plugin Update Nag
//     remove_action( 'load-update-core.php', 'wp_update_plugins' );
//     add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

//   } // endif;

// }


/*-----------------------------------------------------------------------------------*/
/* Javascript enqueue */
/*-----------------------------------------------------------------------------------*/
function toyota_scripts() {  

  wp_register_script('google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAu6GYCwPPra2BBrHkBH4cIgKsQvlqpduE', 'jquery', '', FALSE);
  wp_register_script('google-maps', get_template_directory_uri() . '/js/gmaps.min.js', 'jquery, google-maps-api', '', FALSE);

  wp_enqueue_script('google-maps-api');
  wp_enqueue_script('google-maps');

}

add_action( 'wp_enqueue_scripts', 'toyota_scripts', 99 );


/*-----------------------------------------------------------------------------------*/
/* CPT PROMOTIES */
/*-----------------------------------------------------------------------------------*/
add_action( 'init', 'custom_post_type_promotions' );

function custom_post_type_promotions() {
      
  $labels = array(
    'name' => __('Promoties', 'Toyota Rent'),
    'singular_name' => __('Promotie', 'Toyota Rent'),
    'add_new' => __('Add New', 'Toyota Rent'), __('Vacancy', 'Toyota Rent'),
    'add_new_item' => __('Promotie', 'Toyota Rent'),
    'edit_item' => __('Edit Promotie', 'Toyota Rent'),
    'new_item' => __('Nieuwe Promotie', 'Toyota Rent'),
    'view_item' => __('Bekijk Promotie', 'Toyota Rent'),
    'search_items' => __('Zoek naar promoties', 'Toyota Rent'),
    'not_found' =>  __('Geen promoties gevonden', 'Toyota Rent'),
    'not_found_in_trash' => __('Geen promoties gevonden in Trash', 'Toyota Rent'),
    'parent_item_colon' => ''
  );

  $supports = array(
    'title',
    'editor',
    'categories',
    'excerpt',
    'thumbnail'
  );
  
  register_post_type( 'promotions',
    array(
      'labels' => $labels,
      'public' => true,
      'hierarchical' => false,
      'has_archive' => true,
      'menu_icon' => 'dashicons-megaphone',
      'supports' => $supports
    )
  );
  
}

/*-----------------------------------------------------------------------------------*/
/* CUSTOM SHIPPING OPTIONS */
/*-----------------------------------------------------------------------------------*/

add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_show_extra_profile_fields( $user ) { ?>

  <h3>Delivery prices</h3>

  <table class="form-table">

    <tr>
      <th><label for="shipping_prices">Choose pricing class:</label></th>

      <td>
        <select name="shipping_prices" id="shipping_prices" >
          <option value="klasse1" <?php selected( 'klasse1', get_the_author_meta( 'shipping_prices', $user->ID ) ); ?>>&euro;85 / &euro;170</option>
          <option value="klasse2" <?php selected( 'klasse2', get_the_author_meta( 'shipping_prices', $user->ID ) ); ?>>&euro;50 / &euro;100</option>
          <option value="klasse3" <?php selected( 'klasse3', get_the_author_meta( 'shipping_prices', $user->ID ) ); ?>>&euro;0 / &euro;0</option>
        </select>
      </td>
    </tr>

  </table>
<?php }

add_action( 'personal_options_update', 'save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_profile_fields' );

function save_extra_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;
    update_usermeta( $user_id, 'shipping_prices', $_POST['shipping_prices'] );
}


/*-----------------------------------------------------------------------------------*/
/* Includes */
/*-----------------------------------------------------------------------------------*/
require_once ( 'includes/role.php' );
//require_once ( 'includes/sidebars.php' );
//require_once ( 'includes/widgets.php' );
//require_once ( 'includes/shortcodes.php' );

/*-----------------------------------------------------------------------------------*/
/* Remove and add roles */
/*-----------------------------------------------------------------------------------*/

//remove_role( 'subscriber' );
//remove_role( 'contributor' );
//remove_role( 'editor' );
//remove_role( 'author' );
//add_role( 'fleetowner', 'Fleet Owner', '' );
//add_role( 'dealer', 'Dealer', '' );
//add_role( 'b2b', 'B2B', '' );
?>

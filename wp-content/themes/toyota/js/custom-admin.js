jQuery(document).ready(function($) {
    $('.inventory_tab').remove();  // Inventory
    $('.shipping_tab').remove();  // Shipping
    $('.linked_product_tab').remove(); // Linked Products
    $('.variations_tab').remove(); // Variations
    $('.attributes_tab').remove(); // Attribute
    $('.advanced_tab').remove();  // Advanced
});
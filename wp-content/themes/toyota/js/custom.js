jQuery(document).ready(function($) {
    /*$('.fade').hover(
        function(){
            $(this).find('.caption').fadeIn(250);
            $(this).addClass('hover');
        },
        function(){
            $(this).find('.caption').fadeOut(250);
            $(this).removeClass('hover');
        }
    );*/
    /*************HOVERS & VARIA*********************/
    //Hide cookien notificaiton
    $(".cookieclose").on("click", function(){
      $(".cookies").remove();
      $.cookie('cookienotice', "true");
    });

    //Model thumbs hover state
    $(".model-thumb").mouseover(function(){
      $(this).children(".btn-purple-2").css("background", "#231c23");
    });
    $(".model-thumb").mouseleave(function(){
      $(this).children(".btn-purple-2").css("background", "#2f262f");
    });

    //Toggle registration type
    $("#entity").on("change", function(){
      if($(this).val() === "customer"){
        $("#consumer").show();
        $("#company").hide();
      } else {
        $("#consumer").hide();
        $("#company").show();
      }
    });

    //Check VAT number
    $("#checkvat").on("click", function(){
      var vat = $(this).val();
      vat = vat.replace(/ /g,"");
      vat = vat.replace(/\./g,"");
      window.alert(vat);
      if (vat.match("^BE")) {
           vat = vat.substring(2);
      }
      $.getJSON('http://isvat.appspot.com/BE/'+vat+'/?callback=?', function(data){ 
        if(data === true){ window.alert("is valid"); } 
      });
    });

    //Pick shipping option
    $("#shipping").on("change", function(){
      var shipping_cost = parseInt($(this).val());
      var shipping_class = $("#shipping_class").val();
      var total_cost = parseInt($("#total_cost").val());
      var entity = $("#entity").val();
      if(shipping_class !== "klasse3"){
        total_cost = total_cost + shipping_cost;
        total_cost = total_cost.toFixed(2);
        var total_cost_vat = total_cost * 1.21;
        total_cost_vat = total_cost_vat.toFixed(2);
        $("#total_amount").val("€"+total_cost+" excl. BTW; €"+total_cost_vat+" incl. BTW");
      }
      if(entity === "dealer"){
        var dealer_total_cost = parseInt($("#dealer_total_cost").val());
        var dealer_shipping_cost;
        if(shipping_class === "klasse1" || shipping_class === ""){
          if(shipping_cost === 0){ dealer_shipping_cost = 0; }
          if(shipping_cost === 85){ dealer_shipping_cost = 85; }
          if(shipping_cost === 180){ dealer_shipping_cost = 180; }
        }
        if(shipping_class === "klasse2"){
          if(shipping_cost === 0){ dealer_shipping_cost = 0; }
          if(shipping_cost === 85){ dealer_shipping_cost = 50; }
          if(shipping_cost === 180){ dealer_shipping_cost = 100; }
        }
        if(shipping_class === "klasse3"){
          dealer_shipping_cost = 0;
        }
        dealer_total_cost = dealer_total_cost + dealer_shipping_cost;
        dealer_total_cost = dealer_total_cost.toFixed(2);
        $("#dealer_total_amount").val("€"+dealer_total_cost+" excl. BTW");
      }
    });

    /*************TOGGLES********************/

    $(".toggle_grid").click(function(){
      $(".toggle_grid:first-of-type").hide();
      $(".toggle_list:first-of-type").show();
      $(".grid").show();
      $(".list").hide();
    });
    $(".toggle_list").click(function(){
      $(".toggle_list:first-of-type").hide();
      $(".toggle_grid:first-of-type").show();
      $(".list").show();
      $(".grid").hide();
    });
    $(".to-toggle").click(function(){
      if($(this).siblings(".dealer-price").hasClass('open')){
        $(this).siblings(".dealer-price").hide();
        $(this).siblings(".dealer-price").removeClass('open');  
      } else {
        $(this).siblings(".dealer-price").show();
        $(this).siblings(".dealer-price").addClass('open');        
      }
    });

    /*************DEFAULT SELECT*************/

    $("select").on("change", function(){
      $(this).css("color", "#000");
    });

    /*************PRICE SLIDER***************/

    $( "#price-slider" ).slider({
      range: true,
      min: 0,
      max: 200,
      values: [ 0, 200 ],
      slide: function( event, ui ) {
        $( "#price-slider-from" ).html( " €" + ui.values[ 0 ] );
        $( "#price-slider-to" ).html( " €" + ui.values[ 1 ] );
        $(".model-thumb").each(function(){
          var price_from = $(this).attr("product-price-from");
          var price_to = $(this).attr("product-price-to");
          //Hide values outside of range
          if(price_from <= ui.values[0] || price_to >= ui.values[1]){
            $(this).children('.overlay').show();
          }
          //Show values within range
          if(price_from >= ui.values[0] && price_to <= ui.values[1]){
            $(this).children('.overlay').hide();
          }
        });
        $(".upgrade").each(function(){
          var price_from = $(this).attr("product-price-from");
          var price_to = $(this).attr("product-price-to");
          //Hide values outside of range
          if(price_from <= ui.values[0] || price_to >= ui.values[1]){
            $(this).hide();
          }
          //Show values within range
          if(price_from >= ui.values[0] && price_to <= ui.values[1]){
            $(this).show();
          }
        });
        //Re-apply other filters
        $(".filter").each(function(){
        var c_filter_id = $(this).attr("id");
        var c_filter_value = $(this).val();        
        //Only if filter is not set to defaul or 'All'
        if(c_filter_value !== "0" && c_filter_value !== "" && c_filter_value !== 0 && c_filter_value !== null){          
          if(c_filter_id === "seats" && c_filter_value !== "0"){ c_filter_value = c_filter_value+"-seats"; }
          if(c_filter_id === "doors" && c_filter_value !== "0"){ c_filter_value = c_filter_value+"-doors"; }
          $('.model-thumb').not('[product-filters*="'+c_filter_value+'"]').children('.overlay').show();
          $('.upgrade').not('[product-filters*="'+c_filter_value+'"]').hide();
        }
      });
      }
    });
    $( "#price-slider-from" ).html( " €" + $( "#price-slider" ).slider( "values", 0 ) );
    $( "#price-slider-to" ).html( " €" + $( "#price-slider" ).slider( "values", 1 ) );

    /*************DATEPICKER****************/
    //Set date
    var cookie_from_date = $.cookie('from_date');
    var cookie_end_date = $.cookie('end_date');
    if(cookie_from_date){
      $("#from_date, #start_date").val(cookie_from_date);
    }
    if(cookie_end_date){
      $("#end_date, #till_date").val(cookie_end_date);      
    }

    //Model overview page
    $( "#start_date" ).datepicker({
      minDate: 0,
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat : 'dd/mm/yy',
      onSelect: function( selectedDate ) {
        $( "#end_date" ).datepicker( "option", "minDate", selectedDate );
        $.removeCookie('from_date');
        $.cookie('from_date', selectedDate);
      }
    });
    $( "#end_date" ).datepicker({
      minDate: 1,
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat : 'dd/mm/yy',
      onSelect: function( selectedDate ) {
        $( "#start_date" ).datepicker( "option", "maxDate", selectedDate );
        $.removeCookie('end_date');
        $.cookie('end_date', selectedDate);
      }
    });

    //Model single page
    var max_duration = $("#max_duration").val();

    $( "#from_date" ).datepicker({
      minDate: 0,
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat : 'dd/mm/yy',
      onClose: function( selectedDate ) {
        //Set new minDate based on selected date
        $( "#till_date" ).datepicker( "option", "minDate", selectedDate );
        //Set new maxDate based on maximum duration
        var new_minDate = selectedDate.split("/");
        var new_minDate_parsed = Date.parse(new_minDate[2]+"-"+new_minDate[1]+"-"+new_minDate[0]);
        var max_duration_parsed = max_duration * 86400000;
        var new_maxDate = new_minDate_parsed + max_duration_parsed;
        new_maxDate = new Date(new_maxDate);
        var day = new_maxDate.getDate();
        var month = new_maxDate.getMonth() + 1;
        var year = new_maxDate.getFullYear();
        new_maxDate = day+"/"+month+"/"+year;
        $( "#till_date" ).datepicker( "option", "maxDate", new_maxDate );
        $.removeCookie('from_date');
        $.cookie('from_date', selectedDate);
      }
    });
    $( "#till_date" ).datepicker({
      minDate: 1,
      maxDate: max_duration,
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat : 'dd/mm/yy',
      onClose: function( selectedDate ) {
        //Set new maxDate based on selected date
        $( "#from_date" ).datepicker( "option", "maxDate", selectedDate );
        $.removeCookie('end_date');
        $.cookie('end_date', selectedDate);
      }
    });

    //Registration form
    $( "#consumer_birthdate" ).datepicker({
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat : 'dd/mm/yy',
      onSelect: function() {
        $( ".ui-datepicker a" ).removeAttr("href");
      }
    });

    //Calculate total amount
    $( ".datepicker" ).on('change', function(){
        //Calculate number of days selected
        var product_id = $("#product_id").val();
        var from_date = $("#from_date").val();
        var till_date = $("#till_date").val();
        var vat = $("#vat").val();
        var from_date_arr = from_date.split("/");
        var till_date_arr = till_date.split("/");
        var from_date_parsed = Date.parse(from_date_arr[2]+"-"+from_date_arr[1]+"-"+from_date_arr[0]);
        var till_date_parsed = Date.parse(till_date_arr[2]+"-"+till_date_arr[1]+"-"+till_date_arr[0]);
        var booking_days = (till_date_parsed - from_date_parsed) / 86400000;
        var multiplier = booking_days;

        //Check pricetable for matches based on duration
        var price = parseFloat($("#total_amount").attr("baseprice"));
        var dealer_price;
        $(".pricing_table").each(function(){
          var range = $(this).attr("range"); 
          var from = $(this).attr("from");
          var to = $(this).attr("to");
          if(range === "blocks" && multiplier >= from && multiplier <= to){
            var modifier = $(this).attr("modifier");
            var cost  = parseFloat($(this).attr("value"));
            if(modifier === "") {
              //Add
              price = price + cost;
            }
            if(modifier === "minus") {
              //Subtract
              price = price - cost;
            }
            if(modifier === "times") {
              //Multiply
              price = price * cost;
            }
            if(modifier === "divide") {
              //Divide
              price = price / cost;              
            }
            if(modifier === "replace") {
              //Replace
              price = cost; 
            }
          }
          if(range === "dealer" && multiplier >= from && multiplier <= to){
            var dealer_modifier = $(this).attr("modifier");
            var dealer_cost  = parseFloat($(this).attr("value"));
            if(dealer_modifier === "replace") {
              //Replace
              dealer_price = dealer_cost; 
              $("#dealer_day_price").html("€"+dealer_price);
            }            
          }
        });
//        var sale_pct = $(".pricing_table[range='discount']").value();
        var booking_cost = price * multiplier;
        var booking_cost_vat = price * multiplier * vat;
        booking_cost = booking_cost.toFixed(2);
        booking_cost_vat = booking_cost_vat.toFixed(2);
        if(booking_cost > 0){
          $("#total_amount").val("€"+booking_cost_vat);
        }

        $.removeCookie('product_id');
        $.removeCookie('total_cost');
        $.cookie('product_id', product_id);
        $.cookie('total_cost', booking_cost);

        if(dealer_price > 0){
          var dealer_booking_cost = dealer_price * multiplier;
          dealer_booking_cost = dealer_booking_cost.toFixed(2);
          $("#dealer_total_amount").val("€"+dealer_booking_cost+" excl. BTW");
          $.removeCookie('dealer_total_cost');
          $.removeCookie('dealer_base_cost');
          $.cookie('dealer_total_cost', dealer_booking_cost);
          $.cookie('dealer_base_cost', dealer_price);
        }
    });

    if(cookie_from_date && cookie_end_date){
        var page = $("body").attr("class");
        if (page.indexOf("single-product") >= 0){
          //Calculate number of days selected
          var product_id = $("#product_id").val();
          var from_date = $("#from_date").val();
          var till_date = $("#till_date").val();
          var vat = $("#vat").val();
          var from_date_arr = from_date.split("/");
          var till_date_arr = till_date.split("/");
          var from_date_parsed = Date.parse(from_date_arr[2]+"-"+from_date_arr[1]+"-"+from_date_arr[0]);
          var till_date_parsed = Date.parse(till_date_arr[2]+"-"+till_date_arr[1]+"-"+till_date_arr[0]);
          var booking_days = (till_date_parsed - from_date_parsed) / 86400000;
          var multiplier = booking_days;

          //Check pricetable for matches based on duration
          var price = parseFloat($("#total_amount").attr("baseprice"));
          var dealer_price;
          $(".pricing_table").each(function(){
            var range = $(this).attr("range"); 
            var from = $(this).attr("from");
            var to = $(this).attr("to");
            if(range === "blocks" && booking_days >= from && booking_days <= to){
              var modifier = $(this).attr("modifier");
              var cost  = parseFloat($(this).attr("value"));
              if(modifier === "") {
                //Add
                price = price + cost;
              }
              if(modifier === "minus") {
                //Subtract
                price = price - cost;
              }
              if(modifier === "times") {
                //Multiply
                price = price * cost;
              }
              if(modifier === "divide") {
                //Divide
                price = price / cost;              
              }
              if(modifier === "replace") {
                //Replace
                price = cost; 
              }
            }
            if(range === "dealer" && multiplier >= from && multiplier <= to){
                var dealer_modifier = $(this).attr("modifier");
                var dealer_cost  = parseFloat($(this).attr("value"));
                if(dealer_modifier === "replace") {
                  //Replace
                  dealer_price = dealer_cost; 
                }           
              }
          });

          var booking_cost = price * multiplier;
          var booking_cost_vat = price * multiplier * vat;
          booking_cost = booking_cost.toFixed(2);
          booking_cost_vat = booking_cost_vat.toFixed(2);
          if(booking_cost > 0){
            $("#total_amount").val("€"+booking_cost_vat);
          }

          $.removeCookie('product_id');
          $.removeCookie('total_cost');
          $.cookie('product_id', product_id);
          $.cookie('total_cost', booking_cost);

          if(dealer_price > 0){
            var dealer_booking_cost = dealer_price * multiplier;
            dealer_booking_cost = dealer_booking_cost.toFixed(2);
            $("#dealer_total_amount").val("€"+dealer_booking_cost);
            $("#dealer_day_price").html("€"+dealer_price+" excl. BTW"); 
            $.removeCookie('dealer_total_cost');
            $.removeCookie('dealer_base_cost');
            $.cookie('dealer_total_cost', dealer_booking_cost);
            $.cookie('dealer_base_cost', dealer_price);
          }
        }
    }

    /*************FILTERS*********************/
    //Init existing filters
    $.each($.cookie(), function(filter_id, filter_value) {
      //window.alert(i + " = " + v); // outputs "cookie_name = cookie_value" etc.
      if($("#"+filter_id).hasClass("filter")){
        $(".filter#"+filter_id).val(filter_value).trigger("change");
        if(filter_id === "seats" && filter_value !== "0"){ filter_value = filter_value+"-seats"; }
        if(filter_id === "doors" && filter_value !== "0"){ filter_value = filter_value+"-doors"; }

        //Show all items for this filter
        $("#"+filter_id+" option").each(function(){
          var o_filter_value = $(this).val();      
          $('.model-thumb[product-filters*="'+o_filter_value+'"]').children('.overlay').hide();
          $('.upgrade[product-filters*="'+o_filter_value+'"]').show();
        });

        //Hide all other values for this filter when filter is not 'All'
        if(filter_value !== "0"){
          $('.model-thumb').not('[product-filters*="'+filter_value+'"]').children('.overlay').show();
          $('.upgrade').not('[product-filters*="'+filter_value+'"]').hide();
        }

        //Re-apply other filters
        $(".filter").not('[id*="'+filter_id+'"]').each(function(){
          var c_filter_id = $(this).attr("id");
          var c_filter_value = $(this).val();        
          //Only if filter is not set to defaul or 'All'
          if(c_filter_value !== "0" && c_filter_value !== "" && c_filter_value !== 0 && c_filter_value !== null){          
            if(c_filter_id === "seats" && c_filter_value !== "0"){ c_filter_value = c_filter_value+"-seats"; }
            if(c_filter_id === "doors" && c_filter_value !== "0"){ c_filter_value = c_filter_value+"-doors"; }
            $('.model-thumb').not('[product-filters*="'+c_filter_value+'"]').children('.overlay').show();
            $('.upgrade').not('[product-filters*="'+c_filter_value+'"]').hide();
          }
        });
      }
    });

    //On change event
    $(".filter").on("change", function(){
      var filter_id = $(this).attr("id");
      var filter_value = $(this).val();
      $.removeCookie(filter_id);
      $.cookie(filter_id, filter_value);
      if(filter_id === "seats" && filter_value !== "0"){ filter_value = filter_value+"-seats"; }
      if(filter_id === "doors" && filter_value !== "0"){ filter_value = filter_value+"-doors"; }

      //Show all items for this filter
      $("#"+filter_id+" option").each(function(){
        var o_filter_value = $(this).val();      
        $('.model-thumb[product-filters*="'+o_filter_value+'"]').children('.overlay').hide();
        $('.upgrade[product-filters*="'+o_filter_value+'"]').show();
      });

      //Hide all other values for this filter when filter is not 'All'
      if(filter_value !== "0"){
        $('.model-thumb').not('[product-filters*="'+filter_value+'"]').children('.overlay').show();
        $('.upgrade').not('[product-filters*="'+filter_value+'"]').hide();
      }

      //Re-apply other filters
      $(".filter").not('[id*="'+filter_id+'"]').each(function(){
        var c_filter_id = $(this).attr("id");
        var c_filter_value = $(this).val();        
        //Only if filter is not set to defaul or 'All'
        if(c_filter_value !== "0" && c_filter_value !== "" && c_filter_value !== 0 && c_filter_value !== null){          
          if(c_filter_id === "seats" && c_filter_value !== "0"){ c_filter_value = c_filter_value+"-seats"; }
          if(c_filter_id === "doors" && c_filter_value !== "0"){ c_filter_value = c_filter_value+"-doors"; }
          $('.model-thumb').not('[product-filters*="'+c_filter_value+'"]').children('.overlay').show();
          $('.upgrade').not('[product-filters*="'+c_filter_value+'"]').hide();
        }
      });
      //Re-apply price slider
      var price_filter_from = parseInt($("#price-slider-from").html().substring(2));
      var price_filter_to = parseInt($("#price-slider-to").html().substring(2));
      $(".model-thumb").each(function(){
          var price_from = $(this).attr("product-price-from");
          var price_to = $(this).attr("product-price-to");
          //Hide values outside of range
          if(price_from <= price_filter_from || price_to >= price_filter_to){
            $(this).children('.overlay').show();
          }
      });
      $(".upgrade").each(function(){
          var price_from = $(this).attr("product-price-from");
          var price_to = $(this).attr("product-price-to");
          //Hide values outside of range
          if(price_from <= price_filter_from || price_to >= price_filter_to){
            $(this).hide();
          }
      });

    });

    /*************FORM VALIDATION*********************/

    //Registration form
    $("#register_submit").on("click", function(e){
      e.preventDefault();
      var entity = $("#entity").val();
      if(entity === "customer"){
        var consumer_name = $("#consumer_name").val();
        var consumer_surname = $("#consumer_surname").val();
        var consumer_street = $("#consumer_street").val();
        var consumer_nr = $("#consumer_nr").val();
        var consumer_postal = $("#consumer_postal").val();
        var consumer_city = $("#consumer_city").val();
        var consumer_birthdate = $("#consumer_birthdate").val();
        var consumer_phone = $("#consumer_phone").val();
        var consumer_email = $("#consumer_email").val();
        var consumer_password = $("#consumer_password").val();
        var consumer_password_repeat = $("#consumer_password_repeat").val();
        if(consumer_name.length > 0 && consumer_surname.length > 0 && consumer_street.length > 0 && consumer_nr.length > 0 && consumer_postal.length > 0 && consumer_city.length > 0 && consumer_birthdate.length > 0 && consumer_phone.length > 0 && consumer_email.length > 0 && consumer_password.length > 0 && consumer_password === consumer_password_repeat){
          $("#register_form").submit();
        } else {
          $("#errors").show();
          if(consumer_name.length === 0){ $("#error_name").show();}
          if(consumer_surname.length === 0){ $("#error_surname").show();}
          if(consumer_street.length === 0){ $("#error_street").show();}
          if(consumer_nr.length === 0){ $("#error_nr").show();}
          if(consumer_postal.length === 0){ $("#error_postal").show();}
          if(consumer_city.length === 0){ $("#error_city").show();}
          if(consumer_birthdate.length === 0){ $("#error_birthdate").show();}
          if(consumer_phone.length === 0){ $("#error_phone").show();}
          if(consumer_email.length === 0){ $("#error_email").show();}
          if(consumer_password.length === 0){ $("#error_password").show();}
          if(consumer_password !== consumer_password_repeat){ $("#error_password_match").show(); }
        }
      } else {
        var company_name = $("#company_name").val();
        var company_vat = $("#company_vat").val();
        company_vat = company_vat.replace(/ /g,"");
        company_vat = company_vat.replace(/\./g,"");
        var company_contact_name = $("#company_contact_name").val();
        var company_contact_surname = $("#company_contact_surname").val();
        var company_street = $("#company_street").val();
        var company_nr = $("#company_nr").val();
        var company_postal = $("#company_postal").val();
        var company_city = $("#company_city").val();
        var company_contact_phone = $("#company_contact_phone").val();
        var company_contact_email = $("#company_contact_email").val();
        var company_password = $("#company_password").val();
        var company_password_repeat = $("#company_password_repeat").val();        
        if (company_vat.match("^BE")) {
           company_vat = company_vat.substring(2);
        }

        $.getJSON('http://isvat.appspot.com/BE/'+company_vat+'/?callback=?', function(data){ 
          if(data === true){
            if(company_contact_name.length > 0 && company_contact_surname.length > 0 && company_street.length > 0 && company_nr.length > 0 && company_postal.length > 0 && company_city.length > 0 && company_name.length > 0 && company_contact_phone.length > 0 && company_contact_email.length > 0 && company_password.length > 0 && company_password === company_password_repeat){
              $("#register_form").submit();
            } else {
              window.alert("Form failed");
              $("#errors").show();
              if(company_contact_name.length === 0){ $("#error_name").show();}
              if(company_contact_surname.length === 0){ $("#error_surname").show();}
              if(company_name.length === 0){ $("#error_company").show();}
              if(company_street.length === 0){ $("#error_street").show();}
              if(company_nr.length === 0){ $("#error_nr").show();}
              if(company_postal.length === 0){ $("#error_postal").show();}
              if(company_city.length === 0){ $("#error_city").show();}
              if(company_contact_phone.length === 0){ $("#error_phone").show();}
              if(company_contact_email.length === 0){ $("#error_email").show();}
              if(company_password.length === 0){ $("#error_password").show();}
              if(company_password !== company_password_repeat){ $("#error_password_match").show(); }
            }
          } else {
            window.alert("Gelieve een geldig BTW nummer in te vullen");
            $("#errors").show();
            if(company_contact_name.length === 0){ $("#error_name").show();}
            if(company_contact_surname.length === 0){ $("#error_surname").show();}
            if(company_name.length === 0){ $("#error_company").show();}
            if(company_street.length === 0){ $("#error_street").show();}
            if(company_nr.length === 0){ $("#error_nr").show();}
            if(company_postal.length === 0){ $("#error_postal").show();}
            if(company_city.length === 0){ $("#error_city").show();}
            if(company_contact_phone.length === 0){ $("#error_phone").show();}
            if(company_contact_email.length === 0){ $("#error_email").show();}
            if(company_password.length === 0){ $("#error_password").show();}
            if(company_password !== company_password_repeat){ $("#error_password_match").show(); }
            $("#error_vat").show();
          }
        });
      }
    });

    //Reservation form
    $("#reservation_submit").on("click", function(e){
      e.preventDefault();
      var entity = $("#entity").val();

      var consumer_name = $("#consumer_name").val();
      var consumer_surname = $("#consumer_surname").val();
      var consumer_street = $("#consumer_street").val();
      var consumer_nr = $("#consumer_nr").val();
      var consumer_postal = $("#consumer_postal").val();
      var consumer_city = $("#consumer_city").val();
      var consumer_birthdate = $("#consumer_birthdate").val();
      var consumer_phone = $("#consumer_phone").val();
      var consumer_email = $("#consumer_email").val(); 

      var company_name = $("#company_name").val();
      var company_vat = $("#company_vat").val();      
      company_vat = company_vat.replace(/ /g,"");
      company_vat = company_vat.replace(/\./g,"");
      var company_contact_name = $("#company_contact_name").val();
      var company_contact_surname = $("#company_contact_surname").val();
      var company_street = $("#company_street").val();
      var company_nr = $("#company_nr").val();
      var company_postal = $("#company_postal").val();
      var company_city = $("#company_city").val();
      var company_contact_phone = $("#company_contact_phone").val();
      var company_contact_email = $("#company_contact_email").val();     

      if(entity === "customer"){
        //CONSUMER RESERVATION
        if(consumer_name.length > 0 && consumer_surname.length > 0 && consumer_street.length > 0 && consumer_nr.length > 0 && consumer_postal.length > 0 && consumer_city.length > 0 && consumer_birthdate.length > 0 && consumer_phone.length > 0 && consumer_email.length > 0){
          $("#reservation_form").submit();
        } else {
          $("#errors").show();
          if(consumer_name.length === 0){ $("#error_name").show();}
          if(consumer_surname.length === 0){ $("#error_surname").show();}
          if(consumer_street.length === 0){ $("#error_street").show();}
          if(consumer_nr.length === 0){ $("#error_nr").show();}
          if(consumer_postal.length === 0){ $("#error_postal").show();}
          if(consumer_city.length === 0){ $("#error_city").show();}
          if(consumer_birthdate.length === 0){ $("#error_birthdate").show();}
          if(consumer_phone.length === 0){ $("#error_phone").show();}
          if(consumer_email.length === 0){ $("#error_email").show();}
        }
      } else if(entity === "dealer") {
        //DEALER RESERVATION       
        if(company_vat.match("^BE")) {
           company_vat = company_vat.substring(2);
        }
        if(company_vat === ""){
          var skip_vat_dealer = true;
        }

        $.getJSON('http://isvat.appspot.com/BE/'+company_vat+'/?callback=?', function(data){ 
          if(data === true || skip_vat_dealer === true){
            if(consumer_name.length > 0 && consumer_surname.length > 0 && consumer_street.length > 0 && consumer_nr.length > 0 && consumer_postal.length > 0 && consumer_city.length > 0 && consumer_birthdate.length > 0 && consumer_phone.length > 0 && consumer_email.length > 0 && company_contact_name.length > 0 && company_contact_surname.length > 0 && company_street.length > 0 && company_nr.length > 0 && company_postal.length > 0 && company_city.length > 0 && company_name.length > 0 && company_contact_phone.length > 0 && company_contact_email.length > 0){
              $("#reservation_form").submit();
            } else {
              $("#errors").show();
              if(company_contact_name.length === 0){ $("#error_name").show();}
              if(company_contact_surname.length === 0){ $("#error_surname").show();}
              if(company_name.length === 0){ $("#error_company").show();}
              if(company_street.length === 0){ $("#error_street").show();}
              if(company_nr.length === 0){ $("#error_nr").show();}
              if(company_postal.length === 0){ $("#error_postal").show();}
              if(company_city.length === 0){ $("#error_city").show();}
              if(company_contact_phone.length === 0){ $("#error_phone").show();}
              if(company_contact_email.length === 0){ $("#error_email").show();}
            }
          } else {
            window.alert("Gelieve een geldig BTW nummer in te vullen");
            $("#errors").show();
            if(company_contact_name.length === 0){ $("#error_name").show();}
            if(company_contact_surname.length === 0){ $("#error_surname").show();}
            if(company_name.length === 0){ $("#error_company").show();}
            if(company_street.length === 0){ $("#error_street").show();}
            if(company_nr.length === 0){ $("#error_nr").show();}
            if(company_postal.length === 0){ $("#error_postal").show();}
            if(company_city.length === 0){ $("#error_city").show();}
            if(company_contact_phone.length === 0){ $("#error_phone").show();}
            if(company_contact_email.length === 0){ $("#error_email").show();}
            $("#error_vat").show();
          }
        });        
      } else {
        //FLEET OR B2B RESERVATION
        if (company_vat.match("^BE")) {
           company_vat = company_vat.substring(2);
        }
        if(company_vat === ""){
          var skip_vat = true;
        }

        $.getJSON('http://isvat.appspot.com/BE/'+company_vat+'/?callback=?', function(data){ 
          if(data === true || skip_vat === true){
            if(company_contact_name.length > 0 && company_contact_surname.length > 0 && company_street.length > 0 && company_nr.length > 0 && company_postal.length > 0 && company_city.length > 0 && company_name.length > 0 && company_contact_phone.length > 0 && company_contact_email.length > 0){
              $("#reservation_form").submit();
            } else {
              window.alert("Form failed");
              $("#errors").show();
              if(company_contact_name.length === 0){ $("#error_name").show();}
              if(company_contact_surname.length === 0){ $("#error_surname").show();}
              if(company_name.length === 0){ $("#error_company").show();}
              if(company_street.length === 0){ $("#error_street").show();}
              if(company_nr.length === 0){ $("#error_nr").show();}
              if(company_postal.length === 0){ $("#error_postal").show();}
              if(company_city.length === 0){ $("#error_city").show();}
              if(company_contact_phone.length === 0){ $("#error_phone").show();}
              if(company_contact_email.length === 0){ $("#error_email").show();}
            }
          } else {
            window.alert("Gelieve een geldig BTW nummer in te vullen");
            $("#errors").show();
            if(company_contact_name.length === 0){ $("#error_name").show();}
            if(company_contact_surname.length === 0){ $("#error_surname").show();}
            if(company_name.length === 0){ $("#error_company").show();}
            if(company_street.length === 0){ $("#error_street").show();}
            if(company_nr.length === 0){ $("#error_nr").show();}
            if(company_postal.length === 0){ $("#error_postal").show();}
            if(company_city.length === 0){ $("#error_city").show();}
            if(company_contact_phone.length === 0){ $("#error_phone").show();}
            if(company_contact_email.length === 0){ $("#error_email").show();}
            $("#error_vat").show();
          }
        });
      }
    });
});
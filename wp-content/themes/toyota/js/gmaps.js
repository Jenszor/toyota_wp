if ( document.location.href.indexOf('page_id=12') > -1 ) {
    function enableScrollingWithMouseWheel() {
             map.setOptions({ scrollwheel: true });
        }         
        function disableScrollingWithMouseWheel() {
             map.setOptions({ scrollwheel: false });
        }

        function initialize() {
            var myLatlng = new google.maps.LatLng(50.946599, 4.429731)

            var mapOptions = {
                 center: myLatlng,
                 zoom: 14,
                 disableDefaultUI: true,
                 zoomControl: true,
                 zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                 },
                 scrollwheel:false
            };
            var map = new google.maps.Map(document.getElementById("map-canvas"),
                   mapOptions);

            // To add the marker to the map, use the 'map' property
            var marker = new google.maps.Marker({
                   position: myLatlng,
                   map: map,
                   title:"Toyota Test Days"
            });
         google.maps.event.addListener(map, 'onmousedown', function(){ enableScrollingWithMouseWheel(); });

        }
        google.maps.event.addDomListener(window, 'load', initialize);

        $('body').on('mousedown', function(event) {
            var clickedInsideMap = $(event.target).parents('#map-canvas').length > 0;

            if(!clickedInsideMap) {
                disableScrollingWithMouseWheel();
            }
        });

        $(window).scroll(function() {
            disableScrollingWithMouseWheel();
});
}
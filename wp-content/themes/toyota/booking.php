<?php
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

$product_id = $_POST['product_id'];
$user_id = $_POST['user_id'];
$total_cost = $_POST['total_cost'];
$dealer_total_cost = $_POST['dealer_total_cost'];
$dealer_cost = $_POST['dealer_total_cost'];
$from_date = $_POST['reservation_from_date'];
$from_date_r = $_POST['reservation_from_date'];
$date = DateTime::createFromFormat('d/m/Y', $from_date);
$from_date = $date->format('Y-m-d');
$from_date = strtotime($from_date);
$till_date = $_POST['reservation_till_date'];
$till_date_r = $_POST['reservation_till_date'];
$date = DateTime::createFromFormat('d/m/Y', $till_date);
$till_date = $date->format('Y-m-d');
$till_date = strtotime($till_date);
$entity = $_POST['entity'];
$shipping_class = $_POST['shipping_class'];
$shipping_cost = $_POST['shipping'];

if($entity == "dealer"){
	if($shipping_class == "klasse1" || $shipping_class == ""){
	  if($shipping_cost == 0){ $dealer_shipping_cost = 0; }
	  if($shipping_cost == 85){ $dealer_shipping_cost = 85; }
	  if($shipping_cost == 170){ $dealer_shipping_cost = 170; }
	}
	if($shipping_class == "klasse2"){
	  if($shipping_cost == 0){ $dealer_shipping_cost = 0; }
	  if($shipping_cost == 85){ $dealer_shipping_cost = 50; }
	  if($shipping_cost == 170){ $dealer_shipping_cost = 100; }
	}
	if($shipping_class == "klasse3"){
	  $dealer_shipping_cost = 0;
	}
}
if($shipping_class == "klasse1" || $shipping_class == ""){
  if($shipping_cost == 0){ $shipping_method = "Afhalen bij Toyota Rent"; }
  if($shipping_cost == 85){ $shipping_method = "Wagen laten leveren"; }
  if($shipping_cost == 170){ $shipping_method = "Wagen laten leveren en ophalen"; }
}
if($shipping_class == "klasse2"){
  if($shipping_cost == 0){ $shipping_method = "Afhalen bij Toyota Rent"; }
  if($shipping_cost == 50){ $shipping_method = "Wagen laten leveren"; }
  if($shipping_cost == 100){ $shipping_method = "Wagen laten leveren en ophalen"; }
}
if($shipping_class == "klasse3"){
  if($shipping_cost == 0){ $shipping_method = "Afhalen bij Toyota Rent"; }
  if($shipping_cost == "delivery"){ $shipping_method = "Wagen laten leveren"; }
  if($shipping_cost == "return"){ $shipping_method = "Wagen laten leveren en ophalen"; }
  $shipping_cost = 0;
}


$dealer_total_cost = $dealer_total_cost + $dealer_shipping_cost;
$total_tax = $total_cost * 0.21;



if($entity == "customer"){
	$consumer_name = $_POST['consumer_name'];
	$consumer_surname = $_POST['consumer_surname'];
	$consumer_street = $_POST['consumer_street'];
	$consumer_nr = $_POST['consumer_nr'];
	$consumer_postal = $_POST['consumer_postal'];
	$consumer_city = $_POST['consumer_city'];
	$consumer_birthdate = $_POST['consumer_birthdate'];
	$consumer_phone = $_POST['consumer_phone'];
	$consumer_email = $_POST['consumer_email'];
} elseif($entity == "dealer") {
	$consumer_name = $_POST['consumer_name'];
	$consumer_surname = $_POST['consumer_surname'];
	$consumer_street = $_POST['consumer_street'];
	$consumer_nr = $_POST['consumer_nr'];
	$consumer_postal = $_POST['consumer_postal'];
	$consumer_city = $_POST['consumer_city'];
	$consumer_birthdate = $_POST['consumer_birthdate'];
	$consumer_phone = $_POST['consumer_phone'];
	$consumer_email = $_POST['consumer_email'];
    $company_name = $_POST['company_name'];
    $company_vat = $_POST['company_vat'];
    $company_contact_name = $_POST['company_contact_name'];
    $company_contact_surname = $_POST['company_contact_surname'];
    $company_street = $_POST['company_street'];
    $company_nr = $_POST['company_nr'];
    $company_postal = $_POST['company_postal'];
    $company_city = $_POST['company_city'];
    $company_contact_phone = $_POST['company_contact_phone'];
    $company_contact_email = $_POST['company_contact_email'];
} else {
    $company_name = $_POST['company_name'];
    $company_vat = $_POST['company_vat'];
    $company_contact_name = $_POST['company_contact_name'];
    $company_contact_surname = $_POST['company_contact_surname'];
    $company_street = $_POST['company_street'];
    $company_nr = $_POST['company_nr'];
    $company_postal = $_POST['company_postal'];
    $company_city = $_POST['company_city'];
    $company_contact_phone = $_POST['company_contact_phone'];
    $company_contact_email = $_POST['company_contact_email'];
}

/* CREATE ORDER */
	/*-------------------------------------------------------*/
	// build order data
	if($entity == "dealer"){ $order_notes = "Van: ".$from_date_r." Tot: ".$till_date_r."\rPrijs dealer: &euro;".$dealer_total_cost."\rLeveringsmethode: ".$shipping_method; } else {	$order_notes = "Van: ".$from_date_r." Tot: ".$till_date_r."\rLeveringsmethode: ".$shipping_method; }
	$order_data = array(
	    'post_name'     => 'order', // . date_format($order_date, 'M-d-Y-hi-a'), //'order-jun-19-2014-0648-pm'
	    'post_type'     => 'shop_order',
	    'post_title'    => 'Order &ndash; ', // . date_format($order_date, 'F d, Y @ h:i A'), //'June 19, 2014 @ 07:19 PM'
	    'post_status'   => 'publish',
	    'ping_status'   => 'closed',
	    'post_excerpt'  => $order_notes, //$order->note,
	    'post_author'   => $user_id,
	    'post_password' => uniqid( 'order_' ),   // Protects the post just in case
	    //'post_date'     => date_format($order_date, 'Y-m-d H:i:s e'), //'order-jun-19-2014-0648-pm'
	    'comment_status' => 'open'
	);
	// create order
	$order_id = wp_insert_post( $order_data, true );
	/* ADD DEFAULT WOOCOMMERCE META DATA */
    add_post_meta($order_id, '_order_total', $total_cost + $shipping_cost);
    add_post_meta($order_id, '_customer_user', $user_id);
    add_post_meta($order_id, '_customer_type', $entity);
	add_post_meta($order_id, "_order_shipping", $shipping_cost);
	add_post_meta($order_id, "_order_tax", $total_tax);
	add_post_meta($order_id, "_order_date_from", $from_date_r);
	add_post_meta($order_id, "_order_date_till", $till_date_r);

	if($entity == "dealer"){
	    //add_post_meta($order_id, '_completed_date', date_format( $order_date, 'Y-m-d H:i:s e'), true);
	    //add_post_meta($order_id, '_paid_date', date_format( $order_date, 'Y-m-d H:i:s e'), true);    // billing info
	    add_post_meta($order_id, '_billing_address_1', $company_street, true);
	    add_post_meta($order_id, '_billing_address_2', $company_nr, true);
	    add_post_meta($order_id, '_billing_city', $company_city, true);
	    add_post_meta($order_id, '_billing_postcode', $consumer_postal, true);
	    add_post_meta($order_id, '_billing_email', $company_postal, true);
	    add_post_meta($order_id, '_billing_first_name', $company_contact_name, true);
	    add_post_meta($order_id, '_billing_last_name', $company_contact_surname, true);
	    add_post_meta($order_id, '_billing_phone', $company_contact_phone, true);
	    add_post_meta($order_id, '_billing_vat', $company_vat, true);
	    add_post_meta($order_id, '_shipping_address_1', $consumer_street, true);
	    add_post_meta($order_id, '_shipping_address_2', $consumer_nr, true);
	    add_post_meta($order_id, '_shipping_city', $consumer_city, true);
	    add_post_meta($order_id, '_shipping_postcode', $consumer_postal, true);
	    add_post_meta($order_id, '_shipping_first_name', $consumer_name, true);
	    add_post_meta($order_id, '_shipping_last_name', $consumer_surname, true);
	    add_post_meta($order_id, '_dealer_cost', $dealer_cost, true);
	    add_post_meta($order_id, '_dealer_shipping', $dealer_shipping_cost, true);
	} elseif($entity == "customer"){
	    add_post_meta($order_id, '_billing_address_1', $consumer_street, true);
	    add_post_meta($order_id, '_billing_address_2', $consumer_nr, true);
	    add_post_meta($order_id, '_billing_city', $consumer_city, true);
	    add_post_meta($order_id, '_billing_postcode', $consumer_postal, true);
	    add_post_meta($order_id, '_billing_email', $consumer_postal, true);
	    add_post_meta($order_id, '_billing_first_name', $consumer_name, true);
	    add_post_meta($order_id, '_billing_last_name', $consumer_surname, true);
	    add_post_meta($order_id, '_billing_phone', $consumer_phone, true);
	    add_post_meta($order_id, '_shipping_address_1', $consumer_street, true);
	    add_post_meta($order_id, '_shipping_address_2', $consumer_nr, true);
	    add_post_meta($order_id, '_shipping_city', $consumer_city, true);
	    add_post_meta($order_id, '_shipping_postcode', $consumer_postal, true);
	    add_post_meta($order_id, '_shipping_first_name', $consumer_name, true);
	    add_post_meta($order_id, '_shipping_last_name', $consumer_surname, true);
	}else{
	    //add_post_meta($order_id, '_completed_date', date_format( $order_date, 'Y-m-d H:i:s e'), true);
	    //add_post_meta($order_id, '_paid_date', date_format( $order_date, 'Y-m-d H:i:s e'), true);    // billing info
	    add_post_meta($order_id, '_billing_address_1', $company_street, true);
	    add_post_meta($order_id, '_billing_address_2', $company_nr, true);
	    add_post_meta($order_id, '_billing_city', $company_city, true);
	    add_post_meta($order_id, '_billing_postcode', $company_postal, true);
	    add_post_meta($order_id, '_billing_email', $company_contact_email, true);
	    add_post_meta($order_id, '_billing_first_name', $company_contact_name, true);
	    add_post_meta($order_id, '_billing_last_name', $company_contact_surname, true);
	    add_post_meta($order_id, '_billing_phone', $company_contact_phone, true);
	    add_post_meta($order_id, '_billing_vat', $company_vat, true);
	    add_post_meta($order_id, '_shipping_address_1', $company_street, true);
	    add_post_meta($order_id, '_shipping_address_2', $company_nr, true);
	    add_post_meta($order_id, '_shipping_city', $company_city, true);
	    add_post_meta($order_id, '_shipping_postcode', $company_postal, true);
	    add_post_meta($order_id, '_shipping_first_name', $company_contact_name, true);
	    add_post_meta($order_id, '_shipping_last_name', $company_contact_surname, true);

	}

 	// add item
	$product = get_product($product_id);
	$item_id = wc_add_order_item( $order_id, array(
	     'order_item_name'      => $product->get_title(),
	     'order_item_type'      => 'line_item',
	     'product_id'			=> $product_id,
	) );
	if ( $item_id ) {
		wc_add_order_item_meta( $item_id, '_qty', 1 ); 
	    wc_add_order_item_meta( $item_id, '_tax_class', $product->get_tax_class() );
	    wc_add_order_item_meta( $item_id, '_product_id', $product_id );
	    wc_add_order_item_meta( $item_id, '_line_total', $total_cost );
	    wc_add_order_item_meta( $item_id, '_line_tax', round($total_cost * 0.21, 2) );
	}

/* CREATE BOOKING */
	/*-------------------------------------------------------*/
	// build booking data
	$new_booking = create_wc_booking( 
		$product_id,
		$new_booking_data = array(
			'start_date'  => $from_date, 
			'end_date'    => $till_date,
			'resource_id' => '',
			'user_id'   => $user_id,
			'cost' => $total_cost,
			'all_day' => true
		), 
		'pending', 
		true 
	);

	// get booking id
	$booking_id = $new_booking->id;
	// add order id to booking
	add_post_meta($booking_id, "_booking_order_item_id", $order_id);

header("location: ". get_permalink(103) ." &message=success");

?>
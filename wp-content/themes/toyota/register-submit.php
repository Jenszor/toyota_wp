<?php
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

$entity = $_POST['entity'];
if($entity == "customer"){
	$consumer_name = $_POST['consumer_name'];
	$consumer_surname = $_POST['consumer_surname'];
	$consumer_street = $_POST['consumer_street'];
	$consumer_nr = $_POST['consumer_nr'];
	$consumer_postal = $_POST['consumer_postal'];
	$consumer_city = $_POST['consumer_city'];
	$consumer_birthdate = $_POST['consumer_birthdate'];
	$consumer_phone = $_POST['consumer_phone'];
	$consumer_email = $_POST['consumer_email'];
	$consumer_password = $_POST['consumer_password'];

	$user_id = username_exists( $consumer_email );
	if ( !$user_id and email_exists($consumer_email) == false ) {
		$user_id = wp_create_user( $consumer_email, $consumer_password, $consumer_email );
		update_user_meta( $user_id, 'first_name', $consumer_name );
		update_user_meta( $user_id, 'last_name', $consumer_surname );
		add_user_meta( $user_id, 'billing_first_name', $consumer_name );
		add_user_meta( $user_id, 'billing_last_name', $consumer_surname );
		add_user_meta( $user_id, 'billing_address_1', $consumer_street );
		add_user_meta( $user_id, 'billing_address_2', $consumer_nr );
		add_user_meta( $user_id, 'billing_postcode', $consumer_postal );
		add_user_meta( $user_id, 'billing_city', $consumer_city );
		add_user_meta( $user_id, 'billing_phone', $consumer_phone );
		add_user_meta( $user_id, 'billing_email', $consumer_email );
		add_user_meta( $user_id, 'birthdate', $consumer_birthdate );
		add_user_meta( $user_id, 'shipping_first_name', $consumer_name );
		add_user_meta( $user_id, 'shipping_last_name', $consumer_surname );
		add_user_meta( $user_id, 'shipping_address_1', $consumer_street );
		add_user_meta( $user_id, 'shipping_address_2', $consumer_nr );
		add_user_meta( $user_id, 'shipping_postcode', $consumer_postal );
		add_user_meta( $user_id, 'shipping_city', $consumer_city );
		$u = new WP_User($user_id);
		wp_new_user_notification( $user_id );
		header("location:".get_permalink(21)."&message=success");
	} else {
		header("location:".get_permalink(21)."&message=exists");
	}
} else {
    $company_name = $_POST['company_name'];
    $company_vat = $_POST['company_vat'];
    $company_contact_name = $_POST['company_contact_name'];
    $company_contact_surname = $_POST['company_contact_surname'];
    $company_street = $_POST['company_street'];
    $company_nr = $_POST['company_nr'];
    $company_postal = $_POST['company_postal'];
    $company_city = $_POST['company_city'];
    $company_contact_phone = $_POST['company_contact_phone'];
    $company_contact_email = $_POST['company_contact_email'];
    $company_password = $_POST['company_password'];

	$user_id = username_exists( $company_contact_email );
	if ( !$user_id and email_exists($company_contact_email) == false ) {
		$user_id = wp_create_user( $company_contact_email, $company_password, $company_contact_email );
		update_user_meta( $user_id, 'first_name', $company_contact_name );
		update_user_meta( $user_id, 'last_name', $company_contact_surname );
		add_user_meta( $user_id, 'billing_first_name', $company_contact_name );
		add_user_meta( $user_id, 'billing_last_name', $company_contact_surname );
		add_user_meta( $user_id, 'billing_address_1', $company_street );
		add_user_meta( $user_id, 'billing_address_2', $company_nr );
		add_user_meta( $user_id, 'billing_postcode', $company_postal );
		add_user_meta( $user_id, 'billing_city', $company_city );
		add_user_meta( $user_id, 'billing_phone', $company_contact_phone );
		add_user_meta( $user_id, 'billing_email', $company_contact_email );
		add_user_meta( $user_id, 'billing_company', $company_name );
		add_user_meta( $user_id, 'billing_vat', $company_vat );
		add_user_meta( $user_id, 'shipping_first_name', $company_contact_name );
		add_user_meta( $user_id, 'shipping_last_name', $company_contact_surname );
		add_user_meta( $user_id, 'shipping_address_1', $company_street );
		add_user_meta( $user_id, 'shipping_address_2', $company_postal );
		add_user_meta( $user_id, 'shipping_postcode', $consumer_postal );
		add_user_meta( $user_id, 'shipping_city', $company_city );
		add_user_meta( $user_id, 'shipping_company', $company_name );
		$u = new WP_User($user_id);
		//$u->remove_role( 'customer' );
		//$u->add_role( $entity );
		wp_new_user_notification( $user_id );
		header("location:".get_permalink(21)."&message=success");
	} else {
		header("location:".get_permalink(21)."&message=exists");
	}
}
?>
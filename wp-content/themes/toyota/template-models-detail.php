<?php
/*
Template Name: Models Detail
*/
?>
<?php 
get_header(); 
?>
<div class="row main">
    <div class="container content">

		<div class="product_title clearfix">
			<div class="product_name">
				<h1><span><?php _e("Beschikbare modellen", "Toyota Rent"); ?></span> / 
				<?php if ( !empty( $_GET['model'] ) && !is_wp_error( $$_GET['model'] ) ){ ?>
					<?php $make = get_term_by('slug', $_GET['model'], 'product_cat'); echo $make->name; ?>
				<?php } else { _e("Alle", "Toyota Rent"); } ?>	
				 <span style="font-size:0.6em; margin-left:10px;"><?php if($vat == 1){ _e("excl. BTW", "Toyota Rent"); } else { _e("incl. BTW", "Toyota Rent"); } ?></span>
				</h1></div>
			<div class="toggles">
				<?php if ( !empty( $_GET['model'] ) && !is_wp_error( $$_GET['model'] ) ){ ?>
				<span><img src="<?php bloginfo('template_url'); ?>/images/grid-btn.png" alt="grid" class="toggle_grid hide"/><img src="<?php bloginfo('template_url'); ?>/images/grid-btn-active.png" alt="grid" class="toggle_grid"/></span>
				<span><img src="<?php bloginfo('template_url'); ?>/images/list-btn.png" alt="list" class="toggle_list"/><img src="<?php bloginfo('template_url'); ?>/images/list-btn-active.png" alt="list" class="toggle_list"/></span>
				<?php } ?>
			</div>
	  	</div>

	   <div class="clearfix">

	     <div class="column_two_third">
	        <!-- GRID VIEW -->   
	           <div class="models specific grid">
	           	<div class="clearfix">
	               <!--Content goes here-->
	               <?php
	               	 $model = $_GET['model'];
					 if ( !empty( $model ) && !is_wp_error( $model ) ){
						$args = array(
					        'posts_per_page' => -1,
					        'post_type' => 'product',
					        'product_cat' => $model,
                            'orderby' => 'meta_value_num', 
                            'meta_key' => $price_prefix.'maand',
					        'order' => 'ASC',
					    );
					    $model_query = new WP_Query( $args );
						   if ( $model_query->have_posts() ) :
								while ( $model_query->have_posts() ) : $model_query->the_post();

									$postid = get_the_ID();
									$start_price = get_post_meta( $post->ID, $price_prefix."maand", true );
									$start_price = round($start_price * $vat, 2);
									$product_from_price = get_post_meta( $post->ID, $price_prefix."maand", true );
									$product_from_price = round($product_from_price * $vat, 2);
									$booking_pricing = get_post_meta( $post->ID, "_wc_booking_pricing", false );									
									foreach ($booking_pricing as $key => $booking_pricing_value) {
										foreach ($booking_pricing_value as $key => $value) {
											if( ( $value["type"] == "custom" ) && ( strtotime($value["from"]) <= strtotime("now") ) && ( strtotime($value["to"]) >= strtotime("now") ) ){
												$product_from_price = $product_from_price * $value["cost"];
											}
										}
									}									
									$fuel = strtolower(get_post_meta( $postid, "brandstof", true ));
									$fuel = str_replace(" ", "-", $fuel);
									$transmission = get_post_meta( $postid, "transmissie", true );
									$seats = get_post_meta( $postid, "aantal_zitplaatsen", true );
									$doors = get_post_meta( $postid, "aantal_deuren", true );
									$airco = get_post_meta( $postid, "airco", true );
									if(empty($airco)){ $airco[0] = "no-ocria"; }
									$gps = get_post_meta( $postid, "gps", true );
									if(empty($gps)){ $gps[0] = "no-spg"; }

									//Create DIV
									echo '<div class="model-thumb" product-price-from="'.$product_from_price.'" product-price-to="'.$product_from_price.'" product-filters="'.$fuel.' '.$transmission.' '.$seats.'-seats '.$doors.'-doors '.$airco[0].' '.$gps[0].'">';
					       			echo '<div class="overlay"></div>';					       

					       			$title = str_replace(ucfirst($model)." ", "", get_the_title());
								    $slug = $model->slug;
								    $thumbnail_id = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' );
				    				$image = $thumbnail_id['0'];
				    				$url = get_permalink( $post->ID );
				    				echo '<a href="'.$url.'"><img src="'.$image.'" alt="'.$title.'"/></a>';
				    				echo '<a class="btn btn-purple-2" href="'.$url.'">'.$title.'</a>';

									//Display price
									foreach ($booking_pricing as $key => $booking_pricing_value) {
										foreach ($booking_pricing_value as $key => $value) {
											if( ( $value["type"] == "custom" ) && ( strtotime($value["from"]) <= strtotime("now") ) && ( strtotime($value["to"]) >= strtotime("now") ) ){
												$sale_price = $start_price * $value["cost"];
												echo '<div class="sale_price"><a href="'.$url.'">'.__('Vanaf', 'Toyota Rent').' €'.$start_price.'/dag</a></div>';
												echo '<div class="start_price"><a href="'.$url.'">'.__('Vanaf', 'Toyota Rent').' €'.$sale_price.'/dag</a></div>';							
											}
										}
									}
									if(empty($sale_price)){
										echo '<div class="start_price"><a href="'.$url.'">'.__('Vanaf', 'Toyota Rent').' €'.$start_price.'/dag</a></div>';
									}
									$sale_price = "";
									//Close DIV
					       			echo '</div>';

								endwhile;
							endif;
							wp_reset_postdata();

					 } else {
							woocommerce_get_template_part( 'content', 'model_grid' );
					}
					 ?>
				</div>
	           </div>
	        <!--END GRID VIEW -->  
	        <!--LIST VIEW--> 
	           <div class="models specific list hide">
	           	<div class="clearfix">
	               <!--Content goes here-->
	               <?php

					    $model_query = new WP_Query( $args );
						   if ( $model_query->have_posts() ) :
								while ( $model_query->have_posts() ) : $model_query->the_post();

									woocommerce_get_template_part( 'content', 'product_list' );

								endwhile;
							endif;
							wp_reset_postdata();

					 ?>
				</div>
	           </div>
	        <!--END LIST VIEW-->
	        <div class="promotions">	           
				<?php woocommerce_get_template_part( 'content', 'promotions' ); ?>
			</div>
	     </div>

	     <div class="column_one_third">

	         <div class="content-block blue_box">
					<?php woocommerce_get_template_part( 'content', 'filters' ); ?>
	         </div>

	     </div>

	  </div>
	</div>
</div>
<?php get_footer(); ?>
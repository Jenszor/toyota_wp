<?php
/*
Template name: Homepage
*/
?>

<?php get_header(); ?>

	<?php
	if( have_posts() ) :
		while( have_posts() ) :
			the_post();
	?>

<div class="row main">
    <div class="container">
        
        <h1>Praktisch</h1>

        <ul class="car-list">

            <li>
                <div class="grid-block fade">
                    <div class="caption">
                        <div class="inner">
                            <ul class="car-specs">
                                <li><span class="label red">3NB</span>1.0L 5MT COMFORT</li>
                                <li><span class="label gray">1F7</span>1.3L 6MT DESIGN</li>
                                <li><span class="label white">FF3</span>1.3L CVT LOUNGE</li>
                                <li><span class="label black">00B</span>1.4L 6MT DYNAMIC</li>
                                <li><span class="label gray">B21</span>1.5L HYBRID DYNAMIC<span class="label blue after">hybrid</span></li>
                                <li><span class="label black">00B</span>1.5L HYBRID LOUNGE<span class="label blue after">hybrid</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="thumb">
                        <img src="images/cars/new-aygo.jpg" />
                    </div>
                    <h4>New AYGO</h4>
                </div>
            </li>

            <li>
                <div class="grid-block fade">
                    <div class="caption">
                        <div class="inner">
                            <ul class="car-specs">
                                <li><span class="label red">3NB</span>1.0L 5MT COMFORT</li>
                                <li><span class="label gray">1F7</span>1.3L 6MT DESIGN</li>
                                <li><span class="label white">FF3</span>1.3L CVT LOUNGE</li>
                                <li><span class="label black">00B</span>1.4L 6MT DYNAMIC</li>
                                <li><span class="label gray">B21</span>1.5L HYBRID DYNAMIC<span class="label blue after">hybrid</span></li>
                                <li><span class="label black">00B</span>1.5L HYBRID LOUNGE<span class="label blue after">hybrid</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="thumb">
                        <img src="images/cars/new-yaris.jpg" />
                    </div>
                    <h4>New Yaris</h4>
                </div>
            </li>

            <li>
                <div class="grid-block fade">
                    <div class="caption">
                        <div class="inner">
                            <ul class="car-specs">
                                <li><span class="label red">3NB</span>1.0L 5MT COMFORT</li>
                                <li><span class="label gray">1F7</span>1.3L 6MT DESIGN</li>
                                <li><span class="label white">FF3</span>1.3L CVT LOUNGE</li>
                                <li><span class="label black">00B</span>1.4L 6MT DYNAMIC</li>
                                <li><span class="label gray">B21</span>1.5L HYBRID DYNAMIC<span class="label blue after">hybrid</span></li>
                                <li><span class="label black">00B</span>1.5L HYBRID LOUNGE<span class="label blue after">hybrid</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="thumb">
                        <img src="images/cars/verso-s.jpg" />
                    </div>
                    <h4>Verso-S</h4>
                </div>
            </li>

            <li>
                <div class="grid-block fade">
                    <div class="caption">
                        <div class="inner">
                            <ul class="car-specs">
                                <li><span class="label red">3NB</span>1.0L 5MT COMFORT</li>
                                <li><span class="label gray">1F7</span>1.3L 6MT DESIGN</li>
                                <li><span class="label white">FF3</span>1.3L CVT LOUNGE</li>
                                <li><span class="label black">00B</span>1.4L 6MT DYNAMIC</li>
                                <li><span class="label gray">B21</span>1.5L HYBRID DYNAMIC<span class="label blue after">hybrid</span></li>
                                <li><span class="label black">00B</span>1.5L HYBRID LOUNGE<span class="label blue after">hybrid</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="thumb">
                        <img src="images/cars/auris.jpg" />
                    </div>
                    <h4>Auris</h4>
                </div>
            </li>

            <li>
                <div class="grid-block fade">
                    <div class="caption">
                        <div class="inner">
                            <ul class="car-specs">
                                <li><span class="label red">3NB</span>1.0L 5MT COMFORT</li>
                                <li><span class="label gray">1F7</span>1.3L 6MT DESIGN</li>
                                <li><span class="label white">FF3</span>1.3L CVT LOUNGE</li>
                                <li><span class="label black">00B</span>1.4L 6MT DYNAMIC</li>
                                <li><span class="label gray">B21</span>1.5L HYBRID DYNAMIC<span class="label blue after">hybrid</span></li>
                                <li><span class="label black">00B</span>1.5L HYBRID LOUNGE<span class="label blue after">hybrid</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="thumb">
                        <img src="images/cars/corolla.jpg" />
                    </div>
                    <h4>Corolla</h4>
                </div>
            </li>

            <li>
                <div class="grid-block fade">
                    <div class="caption">
                        <div class="inner">
                            <ul class="car-specs">
                                <li><span class="label red">3NB</span>1.0L 5MT COMFORT</li>
                                <li><span class="label gray">1F7</span>1.3L 6MT DESIGN</li>
                                <li><span class="label white">FF3</span>1.3L CVT LOUNGE</li>
                                <li><span class="label black">00B</span>1.4L 6MT DYNAMIC</li>
                                <li><span class="label gray">B21</span>1.5L HYBRID DYNAMIC<span class="label blue after">hybrid</span></li>
                                <li><span class="label black">00B</span>1.5L HYBRID LOUNGE<span class="label blue after">hybrid</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="thumb">
                        <img src="images/cars/verso.jpg" />
                    </div>
                    <h4>Verso</h4>
                </div>
            </li>

            <li>
                <div class="grid-block fade">
                    <div class="caption">
                        <div class="inner">
                            <ul class="car-specs">
                                <li><span class="label red">3NB</span>1.0L 5MT COMFORT</li>
                                <li><span class="label gray">1F7</span>1.3L 6MT DESIGN</li>
                                <li><span class="label white">FF3</span>1.3L CVT LOUNGE</li>
                                <li><span class="label black">00B</span>1.4L 6MT DYNAMIC</li>
                                <li><span class="label gray">B21</span>1.5L HYBRID DYNAMIC<span class="label blue after">hybrid</span></li>
                                <li><span class="label black">00B</span>1.5L HYBRID LOUNGE<span class="label blue after">hybrid</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="thumb">
                        <img src="images/cars/avensis.jpg" />
                    </div>
                    <h4>Avensis</h4>
                </div>
            </li>

            <li>
                <div class="grid-block fade">
                    <div class="caption">
                        <div class="inner">
                            <ul class="car-specs">
                                <li><span class="label red">3NB</span>1.0L 5MT COMFORT</li>
                                <li><span class="label gray">1F7</span>1.3L 6MT DESIGN</li>
                                <li><span class="label white">FF3</span>1.3L CVT LOUNGE</li>
                                <li><span class="label black">00B</span>1.4L 6MT DYNAMIC</li>
                                <li><span class="label gray">B21</span>1.5L HYBRID DYNAMIC<span class="label blue after">hybrid</span></li>
                                <li><span class="label black">00B</span>1.5L HYBRID LOUNGE<span class="label blue after">hybrid</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="thumb">
                        <img src="images/cars/prius.jpg" />
                    </div>
                    <h4>Prius</h4>
                </div>
            </li>

        </ul>

    </div>
</div>

	<?php
		endwhile;
	endif;
	?>

<?php get_footer(); ?>
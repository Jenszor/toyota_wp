<?php
/*
Template Name: Registration
*/
?>
<?php get_header(); ?>
<div class="row main">
    <div class="container content">
  	   <h1><?php echo get_the_title(); ?></h1>
	   <div class="clearfix">

	     <div class="column_two_third gray_box" style="margin-top:10px;">
	        <div class="content-block">
	        	<form action="<?php bloginfo('template_url'); ?>/register-submit.php" method="post" name="register_form" id="register_form">
	        	<!--
	        	<select id="entity" name="entity">
	        		<option value="b2b"><?php _e("B2B", "Toyota Rent"); ?></option>
	        		<option value="fleetowner"><?php _e("Fleet owner", "Toyota Rent"); ?></option>
	        		<option value="dealer"><?php _e("Concessiehouder", "Toyota Rent"); ?></option>
	        	</select>
	        	-->
	        	<div id="errors">
					<p id="error_name"><?php _e("Gelieve uw voornaam in te vullen", "Toyota Rent"); ?></p>
					<p id="error_surname"><?php _e("Gelieve uw naam in te vullen", "Toyota Rent"); ?></p>
					<p id="error_street"><?php _e("Gelieve uw straat in te vullen", "Toyota Rent"); ?></p>
					<p id="error_nr"><?php _e("Gelieve uw huisnummer in te vullen", "Toyota Rent"); ?></p>
					<p id="error_postal"><?php _e("Gelieve uw postcode in te vullen", "Toyota Rent"); ?></p>
					<p id="error_city"><?php _e("Gelieve uw gemeente in te vullen", "Toyota Rent"); ?></p>
					<p id="error_birthdate"><?php _e("Gelieve uw geboortedatum in te vullen", "Toyota Rent"); ?></p>
					<p id="error_phone"><?php _e("Gelieve uw telefoonnumer in te vullen", "Toyota Rent"); ?></p>
					<p id="error_email"><?php _e("Gelieve uw e-mail adres in te vullen", "Toyota Rent"); ?></p>
					<p id="error_password"><?php _e("Gelieve een wachtwoord in te vullen", "Toyota Rent"); ?></p>
					<p id="error_password_match"><?php _e("De opgegeven wachtwoorden komen niet overeen", "Toyota Rent"); ?></p>

					<p id="error_vat"><?php _e("Het opgegeven BTW nummer is niet correct", "Toyota Rent"); ?></p>
					<p id="error_company"><?php _e("Gelieve uw bedrijfsnaam in te vullen", "Toyota Rent"); ?></p>
	        	</div>
	        	<?php if($_GET['message'] == "success"){ ?> <div class="message"><p><?php _e("Uw registratie is voltooid, u kan nu inloggen.", "Toyota Rent"); ?></p></div><?php } ?>
	        	<?php if($_GET['message'] == "exists"){ ?> <div class="message"><p><?php _e("Een gebruiker met het opgegeven e-mail adres bestaat reeds", "Toyota Rent"); ?></p></div><?php } ?>
	        	<div id="consumer" class="registration">	           
	               <div class="block">
	               	   <p><h2 class="red" style="margin-top:20px;"><?php _e("Persoonlijke gegevens", "Toyota Rent"); ?></h2></p>
		               <p><span><label for="consumer_name"><?php _e("Voornaam", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_name" id="consumer_name"/></span></p>
		               <p><span><label for="consumer_surname"><?php _e("Naam", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_surname" id="consumer_surname"/></span></p>
	               </div>      
	               <div class="block">
		               <p><span><label for="consumer_street"><?php _e("Straat", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_street" id="consumer_street"/></span></p>
		               <p><span><label for="consumer_nr"><?php _e("Huisnummer", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_nr" id="consumer_nr"/></span></p>
		               <p><span><label for="consumer_postal"><?php _e("Postcode", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_postal" id="consumer_postal"/></span></p>
		               <p><span><label for="consumer_city"><?php _e("Gemeente", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_city" id="consumer_city"/></span></p>
		           </div>
	               <div class="block">
		               <p><span><label for="consumer_birthdate"><?php _e("Geboortedatum", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_birthdate" id="consumer_birthdate" class="datepicker"/></span></p>
		               <p><span><label for="consumer_phone"><?php _e("Telefoon", "Toyota Rent"); ?></label></span> <span><input type="text" name="consumer_phone" id="consumer_phone"/></span></p>
		               <p><span><label for="consumer_email"><?php _e("E-mail", "Toyota Rent"); ?></label></span> <span><input type="email" name="consumer_email" id="consumer_email"/></span></p>
	               </div>				
	               <div class="block">
	               	   <p><h2 class="red" style="margin-top:20px;"><?php _e("Wachtwoord", "Toyota Rent"); ?></h2></p>
		               <p><span><label for="consumer_password"><?php _e("Wachtwoord", "Toyota Rent"); ?></label></span> <span><input type="password" name="consumer_password" id="consumer_password"/></span></p>
		               <p><span><label for="consumer_password_repeat"><?php _e("Wachtwoord herhalen", "Toyota Rent"); ?></label></span> <span><input type="password" name="consumer_password_repeat" id="consumer_password_repeat"/></span></p>
	               </div>
	            </div>

		        <div id="company" class="registration">           
	               <div class="block">
	                   <p><h2 class="red" style="margin-top:20px;"><?php _e("Bedrijfsgegevens", "Toyota Rent"); ?></h2></p>
		               <p><span><label for="company_name"><?php _e("Bedrijfsnaam", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_name" id="company_name"/></span></p>
		               <p><span><label for="company_vat"><?php _e("BTW nummer", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_vat" id="company_vat"/></span></p>
	               </div>      
	               <div class="block">
		               <p><span><label for="company_street"><?php _e("Straat", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_street" id="company_street"/></span></p>
		               <p><span><label for="company_nr"><?php _e("Huisnummer", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_nr" id="company_nr"/></span></p>
		               <p><span><label for="company_postal"><?php _e("Postcode", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_postal" id="company_postal"/></span></p>
		               <p><span><label for="company_city"><?php _e("Gemeente", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_city" id="company_city"/></span></p>
		           </div>
	               <div class="block">
	               	   <p><h2 class="red" style="margin-top:20px;"><?php _e("Contactpersoon", "Toyota Rent"); ?></h2></p>
		               <p><span><label for="company_contact_name"><?php _e("Voornaam", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_contact_name" id="company_contact_name"/></span></p>
		               <p><span><label for="company_contact_surname"><?php _e("Naam", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_contact_surname" id="company_contact_surname"/></span></p>
		               <p><span><label for="company_contact_phone"><?php _e("Telefoon", "Toyota Rent"); ?></label></span> <span><input type="text" name="company_contact_phone" id="company_contact_phone"/></span></p>
		               <p><span><label for="company_contact_email"><?php _e("E-mail", "Toyota Rent"); ?></label></span> <span><input type="email" name="company_contact_email" id="company_contact_email"/></span></p>
	               </div>					
	               <div class="block">
	              	   <p><h2 class="red" style="margin-top:20px;"><?php _e("Wachtwoord", "Toyota Rent"); ?></h2></p>
		               <p><span><label for="company_password"><?php _e("Wachtwoord", "Toyota Rent"); ?></label></span> <span><input type="password" name="company_password" id="company_password"/></span></p>
		               <p><span><label for="company_password_repeat"><?php _e("Wachtwoord herhalen", "Toyota Rent"); ?></label></span> <span><input type="password" name="company_password_repeat" id="company_password_repeat"/></span></p>
	               </div>	            
		        </div>
		        <button type="submit" id="register_submit" class="btn-purple-2" value="<?php _e("Registreer", "Toyota Rent"); ?>"><?php _e("Registreer", "Toyota Rent"); ?></button>
		        </form>
	        </div>

	     </div>

	     <div class="column_one_third">

	         <div class="content-block blue_box">
	               <h2><?php _e("Openingsuren", "Toyota Rent"); ?></h2>
	               <table class="contact_table">
	                   <tr>
	                       <td><?php _e("Maandag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "maandag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Dinsdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "dinsdag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Woensdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "woensdag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Donderdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "donderdag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Vrijdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "vrijdag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Zaterdag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "zaterdag", true); ?></td>
	                   </tr>
	                   <tr>
	                       <td><?php _e("Zondag", "Toyota Rent"); ?></td>
	                       <td style="text-align: right;"><?php echo get_post_meta( 12 , "zondag", true); ?></td>
	                   </tr>
	               </table>
	           </div>

	     </div>

	  </div>
	</div>
</div>
<?php get_footer(); ?>